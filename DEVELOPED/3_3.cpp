//
//  3_3.cpp
//  HOME
//
//  Created by Твердый Евгений on 20.12.13.
//  Copyright (c) 2013 Твердый Евгений. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

ifstream fin("tree.in");
ofstream fout("tree.out");

void ULR(string LUR, string LRU)
{
    if (LUR.length())
    {
        size_t root = 0;
        size_t found = 0;
        for (int i = 0; i < LUR.length(); ++i)
        {
            size_t now = LRU.find(LUR[i]);
            if (now > root)
            {
                root = now;
                found = i;
            }
        }
        cout << LUR[found] << endl;
        fout << LUR[found];
        LRU.erase(root, 1);
        LUR.erase(found, 1);

        ULR(LUR.substr(0, found), LRU);
        ULR(LUR.substr(found, LUR.length() - found), LRU);
    }
}

int main()
{
    string LUR = "EKACBLDNM";//На всякий случай
    string LRU = "KEALNMDBC";//На всякий случай
    
    if (fin.is_open())
    {
        fin >> LUR;
        fin >> LRU;
    }
    
    
    ULR(LUR, LRU);
    cout << endl;
    return 0;
}