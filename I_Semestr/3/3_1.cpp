#include <iostream>
#include <stdio.h>

using namespace std;

int area(int a)
{
	return a*a*a;
}

int area(int a, int b)
{
	return a*a*b;
}
int area(int a, int b, int c)
{
	return a*b*c;
}

int main()
{

	cout<<area(2)<<endl;
	cout<<area(2,3)<<endl;
	cout<<area(2,3,4)<<endl;

	//system("pause");
	return 0;
}