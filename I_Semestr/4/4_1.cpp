#include <iostream>
#include <cstdlib>
using namespace std;

template <typename T>
 void ypor(T &a, T &b, T &c)
 {
	 if (b < a) swap(a,b);
	 if (c < a) 
	 {
		 swap(a,c);
		 swap(c,b);
	 }
	 else 
	 {
		 if (c < b)
		 {
			 swap(b,c);
		 }
	 }
 }
 int main()
 {
	 int a, b, c;
	 cin>>a>>b>>c;
	 ypor(a,b,c);
	 cout<<a<<b<<c<<endl;
	 return 0;
 }