#include <iostream>
#include <stdio.h>

using namespace std;
template <typename T>

void Swap(T &a, T &b)
{
#ifdef _DEBUG
	T c = b;
	b = a;
	a = c;
#else 
	swap(a,b);
#endif
}

int main()
{
	int a, b;
	cin>>a>>b;
	Swap(a,b);
	cout<<a<<" "<<b<<endl;
	system("pause");
	return 0;
}