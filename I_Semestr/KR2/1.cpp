#include <iostream>
#include <stdio.h>
using namespace std;

int f()
{
	static int p = 1;
	int i = (p + 2) / 2;
	if ((p != 1) && (p != 2))
	{
		int i = p / 2 + 1; 
		while(1)
		{
			if (p % i == 0) 
			{
				p++;
				i = p / 2 + 1;
			}
			else 
			{
				i--;
				if (i == 1) 
				{
					p++;
					return p - 1;
				}
			}
		}
	}
	else 
	{
		p++;
		return p - 1;
	}
}

int main()
{
	cout<<f()<<endl;
	cout<<f()<<endl;
	cout<<f()<<endl;
	cout<<f()<<endl;
	cout<<f()<<endl;
	cout<<f()<<endl;
	cout<<f()<<endl;


	system("pause");
	return 0;
}