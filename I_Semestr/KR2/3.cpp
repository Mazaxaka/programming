#include <iostream>
#include <stdio.h>
using namespace std;

int f(int n)
{
	int ans = 1, test = 1;
	int ogr = 1;
	for (int i = 0; i < n; i++)
		ogr *= 2;
	ogr --;
	while (test <= ogr)
	{
		int te = test;
		while (te)
		{
			if ((te % 2 == 1) && ((te/2) % 2 == 1)) 
			{
				test++;
				break;
			}
			else
			{
				te /= 2;
				if (te == 0) 
				{
					ans ++;
					test++;
					break;
				}
			}
		}
	}
	return ans;
}



int main()
{
	for (int i = 0; i < 10; i++)
	cout<<f(i+1)<<endl;
	system("pause");
	return 0;
}