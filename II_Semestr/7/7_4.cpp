#include <iostream>
using namespace std;

template <typename T>
void buble_sort(T inp[], int len)
{
    bool quit = true;
    for (int i = 0; i < len; ++i)
    {
        for (int j = 0; j < len - i - 1; ++j)
        {
            if (inp[j] > inp[j+1])
            {
                swap(inp[j], inp[j+1]);
                quit = false;
            }
        }
        if (quit) return;
		quit = true;
    }
}

int main()
{
    int n, a[100];
    cin >> n;
    for (int i = 0; i < n; ++i)
        cin >> a[i];
    buble_sort(a,n);
    for (int i = 0; i < n; ++i)
        cout << a[i] << " ";
    return 0;
}
