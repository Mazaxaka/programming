#include <iostream>
using namespace std;

bool i_bit(long long inp, int bit)
{
	return (inp >> bit) % 2;
}
void bit_sort(long long inp[], int n, int k)
{
	long long **temp = new long long* [2];
	for (int i = 0; i < 2; ++i)
	{
		temp[i] = new long long [n];
		for (int j = 0; j < n; ++j)
			temp[i][j] = -1;
	}
	int bit = 0;
	bool OK = false;
	while (!OK)
	{
		for (int i = 0; i < n; ++i)
		{
			temp[i_bit(inp[i], bit)][i] = inp[i];
		}

		int now = 0;
		OK = true;
		for (int i = 0; i < 2; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				if(temp[i][j] != -1)
				{
					inp[now] = temp[i][j];
					if (now)
					{
						if (inp[now] < inp[now - 1]) OK = false;
					}
					temp[i][j] = -1;
					++now;
				}
			}
		}
		++bit;
	}

	for (int i = 0; i < 2; ++i)
		delete [] temp[i];
	delete [] temp;

}
int main()
{
	long long n;
	cin >> n;
	long long inp[100];
	for (int i = 0; i < n; ++i)
		cin >> inp[i];
	bit_sort(inp,n,10);
	for (int i = 0; i < n; ++i)
		cout << inp[i] << " ";

	system("pause"); 
	return 0;
}
