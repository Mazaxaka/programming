#include <iostream>
using namespace std;
int digit_i(unsigned long long a, int digit)
{
	for (int i = 0; i < digit; ++i)
	{
		a /= 10;
	}
	return a % 10;
}
void block_sort(long long inp[], int n)
{
	
	bool ok = false;
	int digit = 0;
	while (!ok)
	{
		long long temp[10][100];
		for (int i = 0; i < 10; ++i)
			for (int j = 0; j < n; ++j)
				temp[i][j] = -1;
		for (int i = 0; i < n; ++i)
		{
			temp[digit_i(inp[i], digit)][i] = inp[i];
		}
		ok = true;
		int now = 0;
		for (int j = 0; j < 10; ++j)
		{
			for (int i = 0; i < n; ++i)
			{
				if (temp[j][i] != -1)
				{
					inp[now++] = temp[j][i];
					if (now > 1)
					{
						if (inp[now-1] < inp[now-2])
						{
							ok = false;
						}
					}
				}
			}

		}
		++digit;
	}

}
int main()
{
	int n;
	cin >> n;
	long long a[100];
	for (int i = 0; i < n; ++i)
	{
		cin >> a[i];
	}
	block_sort(a,n);
	for (int i = 0; i < n; ++i)
	{
		cout << a[i] << " ";
	}
	cout << endl;
	system("pause");
	return 0;
}