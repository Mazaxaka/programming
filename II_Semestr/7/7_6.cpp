#include <iostream>
#include <vector>
using namespace std;
const int INF = 1E9;
template <typename T>
void merge(T inp[], int left, int middle, int right)
{
    int n1 = middle - left + 1;
    int n2 = right - middle;
    vector <T> L(n1 + 1);
    vector <T> R(n2 + 1);
    for (int i = 0; i < n1; ++i)
    {
        L[i] = inp[left + i];
    }
    for (int i = 0; i < n2; ++i)
    {
        R[i] = inp[middle + i + 1];
    }
    L[n1] = INF;
    R[n2] = INF;
    for (int k = left, i = 0, j = 0; k <= right; ++k)
    {
        if (L[i] <= R[j])
        {
            inp[k] = L[i];
            ++i;
        }
        else
        {
            inp[k] = R[j];
            ++j;
        }
    }
}
template <typename T>
void merge_sort(T inp[], int from, int to)
{
    if (from >= to) return;
    int middle = (from + to) / 2;
    merge_sort(inp, from , middle);
    merge_sort(inp, middle + 1, to);
    merge(inp, from, middle, to);

}

int main()
{
    int n, a[100];
    cin >> n;
    for (int i = 0; i < n; ++i)
        cin >> a[i];
    merge_sort(a,0, n - 1);
    for (int i = 0; i < n; ++i)
        cout << a[i] << " ";
	system("pause");
    return 0;
}
