#include <iostream>

using namespace std;
template<typename T>
void quick_sort(T inp[], int from, int to)
{
	int bea_numb = int(rand()) % (to - from) + from;
	int bearing = inp[bea_numb];
	int now_i = from; 
	int now_j = to;
	do
	{
		while (inp[now_i] < bearing)
			++now_i;
		while (inp[now_j] > bearing)
			--now_j;
		if (now_i <= now_j) 
			swap(inp[now_i++], inp[now_j--]);
	}
	while (now_i < now_j);
	if (from < now_j) quick_sort(inp, from, now_j);
	if (now_i < to) quick_sort(inp, now_i, to);
}
int main()
{
	int n;
	cin >> n;
	int a[100];
	for (int i = 0; i < n; ++i)
		cin >> a[i];
	quick_sort(a, 0, n - 1);
	for (int i = 0; i < n; ++i)
		cout << a[i] << " ";
	system("pause");
	return 0;

}