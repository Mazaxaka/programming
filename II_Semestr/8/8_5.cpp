#include <iostream>
using namespace std;

template<typename T>
class stack
{
	struct stack_node
	{
		T value;
		stack_node* prev;
	};
	stack_node* Head;
	
public:
	stack()
	{
		Head = NULL;
	}
	void push(T value)
	{
		if (Head == NULL)
		{
			Head = new stack_node;
			Head->prev = 0;
			Head->value = value;
		}
		else 
		{
			stack_node* temp = new stack_node;
			temp->prev = Head;
			Head = temp;
			Head->value = value;
		}
	}
	int pop()
	{
		if (Head != NULL)
		{
			T ans = Head->value;
			stack_node* temp = Head;
			Head = Head -> prev;
			delete temp;
			return ans;
		}
		else 
			return -1;
	}
	~stack()
	{
		while (Head!= NULL)
		{
			stack_node *temp = Head-> prev;
			delete Head;
			Head = temp;
		}
	}
};

int main()
{

	stack<char> MyStack;
	char c;	
	c = getchar();
	while (c != 10)
	{
		
		if (c == '(' || c == '{' || c == '[')
			MyStack.push(c);
		else 
		{
			char temp = MyStack.pop();
			if (c == ')' && temp == '(' || c == '}' && temp == '{' || c == ']' && temp == '[');
			else 
			{
				cout << "not correct" <<endl;
				system("pause");
				return 0;
			}
		}
		c = getchar();
	}
	if (MyStack.pop() == -1)
		cout << "correct" << endl;
	else 
		cout << "not correct" <<endl;
	system("pause");
	return 0;
}