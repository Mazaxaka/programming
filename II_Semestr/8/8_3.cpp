#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

unsigned int min(unsigned int a, unsigned int b)
{
	if (b <= a) return b;
	else return  a;
}
unsigned int NOD (unsigned int a, unsigned int b)
{
	unsigned int m = a;
	unsigned int n = b;
	unsigned int k = b;
	while (!(m == 0 || n == 0))
	{
		if (m > n) m -= n;
		else n -=m;
		if (m == 0)
			k = n;
		else
			k = m;
	}
	return k;
}

void first_min(unsigned int a, unsigned int b)
{
	if (a > b) swap(a,b);
}
unsigned int NOK (unsigned int a, unsigned int b)
{
	return a * b / NOD(a, b);
}

class frac
{
public :
	int num;
	unsigned int den;

	frac()
	{
		num = 0;
		den = 1;
	}
	frac(int a)
	{
		num = a;
		den = 1;
	}
	frac(int a, unsigned int b)
	{
		num = a;
		den = b;
	}

	void frac_red()
	{
		unsigned int NO = NOD(abs(num), den);
		num /= NO;
		den /= NO;
	}
	const frac operator+(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * nok / den + b.num * nok / b.den;
		ans.frac_red();
		return ans;
	}
	const frac operator-(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * int (nok / den) - b.num * int (nok / b.den);
		ans.frac_red();
		return ans;
	}
	const frac operator*(const frac& b)
	{
		frac ans;
		ans.num = num * b.num;
		ans.den = den * b.den;
		ans.frac_red();
		return ans;
	}
	const frac operator/(const frac& b)
	{
		frac ans;
		ans.num = num * b.den;
		ans.den = den * b.num;
		ans.frac_red();
		return ans;
	}
	const void operator=(const frac& b)
	{
		num = b.num;
		den = b.den;
	}
	const bool operator==(const frac& b)
	{
		if (num == b.num && den == b.den) return 1;
		return 0;
	}
	const bool operator<(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * nok / den - b.num * nok / b.den;
		ans.frac_red();
		if (ans.num < 0) return 1;
		else return 0;
	}
	const bool operator>(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * nok / den - b.num * nok / b.den;
		ans.frac_red();
		if (ans.num > 0) return 1;
		return 0;
	}
};


int main()
{
	int a1, a2;
	unsigned int b1 , b2;
	cin >> a1 >> b1;
	frac a(a1, b1);
	cin >> a2 >> b2;
	frac b(a2,b2);
	frac c = a + b;
	cout << c.num << " / " << c.den << endl;
	c = a - b;
	cout << int(c.num) << " / " << c.den << endl;
	c = a * b;
	cout << c.num << " / " << c.den << endl;
	c = a / b;
	cout << c.num << " / " << c.den << endl;
	system("pause");
	return 0;
}
