#include <iostream>

using namespace std;
template <class T>
class deque
{
	struct deque_element
	{
		deque_element *next, *prev;
		T value;
	};
	deque_element *Head, *Tail;
public:
	deque()
	{
		Tail = NULL;
		Head = NULL;
	}
	void insert_tail (T value)
	{
		if (Tail == NULL && Head == NULL)
		{
			Tail = new deque_element;
			Head = Tail;
			Tail -> next = NULL;
			Tail -> prev = NULL;
			Tail -> value = value;
		}
		else 
		{
			Tail -> next = new deque_element;
			Tail -> next -> prev = Tail;
			Tail = Tail -> next;
			Tail -> value = value;
			Tail -> next = NULL;
		}
	}
	void insert_head(T value)
	{
		if (Tail == NULL && Head == NULL)
		{
			Head = new deque_element;
			Tail = Head;
			Head -> next = NULL;
			Head -> prev = NULL;
			Head -> value = value;
			
		}
		else 
		{
			deque_element *temp = new deque_element;
			temp -> next = Head;
			Head -> prev = temp;
			Head = temp;
			Head -> value = value;
			Head -> prev = NULL;
		}
	}
	void delete_tail()
	{
		
		if (Tail -> prev == NULL)
		{
			delete Tail;
			Tail = Head = NULL;
		}
		else 
		{
			Tail = Tail -> prev;
			delete (Tail -> next);
			Tail -> next = NULL;
		}
	}
	void delete_head()
	{
		if (Head -> next == NULL)
		{
			delete Head;
			Tail = Head = NULL;
		}
		else 
		{
			Head = Head -> next;
			delete (Head -> prev);
			Head -> prev = NULL;
		}
	}
	T show_head()
	{
		return Head -> value;
	}
	T show_tail()
	{
		return Tail -> value;
	}
};
int main()
{
	deque<int> deq;
	deq.insert_head(3);
	deq.insert_head(2);
	deq.insert_head(1);
	deq.insert_tail(4);
	deq.insert_tail(5);

	cout << deq.show_tail() << endl;
	cout << deq.show_head() << endl;

	deq.delete_head();
	deq.delete_head();
	deq.delete_head();
	deq.delete_head();
	deq.delete_head();

	deq.insert_head(3);
	deq.insert_head(2);
	deq.insert_head(1);
	deq.insert_tail(4);
	deq.insert_tail(5);

	deq.delete_tail();
	deq.delete_tail();
	deq.delete_tail();
	deq.delete_tail();
	deq.delete_tail();
	system("pause");
	return 0;
}
