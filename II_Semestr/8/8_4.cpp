#include <iostream>

using namespace std;
const int Max = 20;

class Polinomial
{
	int max_step;
	double koff[Max];
	friend Polinomial sum(Polinomial a, Polinomial b);
	
public:
	friend Polinomial operator+(const Polinomial &a, const Polinomial &b);
	friend Polinomial operator-(const Polinomial &a, const Polinomial &b);
	friend Polinomial operator*(const Polinomial &a, const Polinomial &b);
	Polinomial()
	{
		for (int i = 0; i < Max; ++i)
		{
			koff[i] = 0;
		}
		max_step = 0;
	}
	~Polinomial()
	{

	}
	double value(double val)
	{
		double ans = 0;
		for (int i = 0; i <= max_step; ++i)
		{
			ans += koff[i] * pow(val, i);
		}
		return ans;
	}
	void set_coef(int step, double coef)
	{
		koff[step] = coef;
		if (step > max_step) max_step = step;
	}
	void out()
	{
		for (int i = max_step; i >= 0; --i)
		{
			if (i == max_step && koff[i] == 1 && i == 1) cout << "x";
			else if (i == max_step && koff[i] == 1 && i != 0) cout << koff[i] << "x^" << i;
			else if (i == max_step && i == 1) cout << koff[i] << "x";
			else if (i == max_step && i != 0) cout << koff[i] << "x^" << i;
			else if (koff[i] == 1 && i == 1) cout << " + x";
			else if (koff[i] > 0 && i == 1) cout << " + " << koff[i] << "x";
			else if (koff[i] < 0 && i == 1) cout << " " << koff[i] << "x";
			else if (koff[i] == 1 && i != 0) cout << " + x^" << i;
			else if (koff[i] > 0 && i != 0) cout << " + " << koff[i] << "x^" << i;
			else if (koff[i] < 0) cout << koff[i] << "x^" << i;
			else if (koff[i] == 0);
			else if (i == i) cout << " + " << koff[i];
		}
		cout << endl;
	}


};
Polinomial sum(Polinomial a, Polinomial b)
{
	Polinomial ans;
	for (int i = 0; i <= max(a.max_step,b.max_step); ++i)
	{
		ans.set_coef(i, a.koff[i] + b.koff[i]);
	}
	return ans;
}
Polinomial operator+(const Polinomial &a, const Polinomial &b)
{
	Polinomial ans;
	for (int i = 0; i <= max(a.max_step, b.max_step); ++i)
	{
		ans.set_coef(i, a.koff[i] + b.koff[i]);
	}
	return ans;
}
Polinomial operator-(const Polinomial &a, const Polinomial &b)
{
	Polinomial ans;
	for (int i = 0; i <= max(a.max_step, b.max_step); ++i)
	{
		ans.set_coef(i, a.koff[i] - b.koff[i]);
	}
	return ans;
}
Polinomial operator*(const Polinomial &a, const Polinomial &b)
{
	Polinomial ans;
	for (int i = 0; i <= a.max_step; ++i)
	{
		for (int j = 0; j <= b.max_step; ++j)
		{
			if (i + j <  Max) 
				ans.set_coef(i + j, ans.koff[i + j] + a.koff[i] * b.koff[j]);
		}
	}
	return ans;
}


int main()
{
	Polinomial a, b, c;
	a.set_coef(0, 3);
	a.set_coef(1, -2);
	/*a.set_coef(2, 1);
	a.set_coef(3, 4);
	a.set_coef(4, 11);
	a.set_coef(5, 63);*/
	b.set_coef(1, 17);
	/*b.set_coef(2, 3);
	b.set_coef(4, 5);
	b.set_coef(5, 1);
	b.set_coef(8, 9);*/
	a.out();
	b.out();
	cout <<"a(3) = " << a.value(3) << endl;
	cout <<"b(3) = " << b.value(3) << endl;
	c = a * b;
	c.out();
	c = a + b;
	c.out();
	c = a - b;
	c.out();
	system("pause");
	return 0;
}