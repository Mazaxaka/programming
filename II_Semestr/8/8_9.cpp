#include <iostream>

using namespace std;

template <class T>
class Tree
{
	struct tree_node
	{
		tree_node *right_child, *left_child;
		T value;
	};
	tree_node *root;
	int heigh;
public:
	Tree()
	{
		root = NULL;
		heigh = -1;
	}
	~Tree()
	{
		if(root!=NULL) free(root);
	}
	void free(tree_node* tree)
	{
		if (tree->left_child != NULL) free(tree->left_child);
		if (tree->right_child != NULL) free(tree->right_child);
		delete tree;
		tree = NULL;
	}
	void push_value(tree_node* &tree, T value, int &now_hei)
	{
		if (tree == NULL)
		{
			tree =  new tree_node;
			tree->value = value;
			tree->right_child = NULL;
			tree->left_child = NULL;
		}
		else if (value >= tree->value)
		{
			push_value(tree->right_child, value,now_hei);
		}
		else if (value < tree->value)
		{
			push_value(tree->left_child, value,now_hei);
		}
		++now_hei;
	}
	void push(T value)
	{
		int now_hei = -1;
		push_value(root, value, now_hei);
		if (now_hei > heigh) heigh = now_hei;

	}
	void array_sort(tree_node * tree)
	{
		if (tree->left_child != NULL) array_sort(tree->left_child);
		cout << tree->value << " ";
		if (tree->right_child != NULL) array_sort(tree->right_child);
	}
	void sort()
	{
		if (root != NULL)
			array_sort(root);
	}
	int my_heigh()
	{
		return heigh;
	}
	
};

int main()
{
	Tree<int> tree;
	FILE *fp;
	fp = fopen("input.txt", "r");
	int n;
    fscanf(fp, "%d", &n);
	for (int i = 0; i < n; ++i)
	{
		int a;
		fscanf(fp, "%d", &a);
		tree.push(a);
	}
	fclose(fp);
	freopen("output.txt","w", stdout);
	tree.sort();
	cout << endl << "h = " << tree.my_heigh();
	fclose(stdout);
	return 0;
}