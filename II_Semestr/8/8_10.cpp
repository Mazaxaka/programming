#include <iostream>
using namespace std;

//template <class T>
class Tree
{
	struct tree_node
	{
		tree_node *right_child, *left_child;
		int value;
		int number;
	};
	tree_node *root;
	int height;
public:
	int last;
	Tree()
	{
		root = NULL;
		height = -1;
		last = -1;
	}
	~Tree()
	{
		if(root!=NULL) free(root);
	}
	void free(tree_node* tree)
	{
		if (tree->left_child != NULL) free(tree->left_child);
		if (tree->right_child != NULL) free(tree->right_child);
		delete tree;
		tree = NULL;
	}
	void push_number(tree_node* &tree, int number, int value, int &now_hei)
	{
		if (tree == NULL)
		{
			tree =  new tree_node;
			tree->number = number;
			tree->value = value;
			tree->right_child = NULL;
			tree->left_child = NULL;
		}
		else if (number >= tree->number)
		{
			push_number(tree->right_child, number, value,now_hei);
		}
		else if (number < tree->number)
		{
			push_number(tree->left_child, number, value,now_hei);
		}
		++now_hei;
	}
	void push(int number, int value)
	{
		int now_hei = -1;
		push_number(root, number, value, now_hei);
		if (now_hei > height) height = now_hei;

	}
	void array_sort(tree_node * tree)
	{
		if (tree->left_child != NULL) array_sort(tree->left_child);
		for (int i = 0; i < tree->number - last - 1; ++i)
			cout << 0 << " ";
		last = tree->number;
		cout << tree->value << " ";
		if (tree->right_child != NULL) array_sort(tree->right_child);
	}
	void array_sort_to_array(tree_node * tree, int a[], int &now)
	{
		if (tree->left_child != NULL) array_sort_to_array(tree->left_child, a, now);
		for (int i = 0; i < tree->number - last - 1; ++i)
			a[now++] = 0;
		last = tree->number;
		a[now] = tree->value;
		if (tree->right_child != NULL) array_sort(tree->right_child, a, now);
	}
	void sort()
	{
		if (root != NULL)
			array_sort(root);
	}
	void sort_to_array(int a[])
	{
		int now = 0;
		if (root != NULL)
			array_sort_to_array(root, a, now);
	}
	int my_height()
	{
		return height;
	}
	
};
class matrix
{
	Tree a[100];
	int maxlen;
	int height;
public:
	matrix()
	{
		maxlen = 0;
	}
	void fill()
	{
		cout << "Please enter the number of rows: "; 
		cin >> height;
		for (int i = 0; i < height; ++i)
		{
			int length;
			cout << "Please enter the number of elements in a row " << i << ": ";
			cin >> length;
			if (length) 
				cout << "Enter an item number - space - value: ";
			for (int j = 0; j < length; ++j)
			{
				int numb;
				int value;
				cin >> numb >> value;
				if (numb > maxlen) maxlen = numb;
				a[i].push(numb, value);
			}
		}
		
	}
	void sum(matrix A, matrix B)
	{
		for (int i = 0; i < height; ++i)
		{
			int *k = new int [maxlen];
			int *b = new int [maxlen];
			A.a[i].sort_to_array(k);
			B.a[i].sort_to_array(b);
			for (int i = 0; i < maxlen; ++i)
				if (k[i] != 0 && b[i] != 0)
				{
					a[i].push(i, k[i] + b[i]);
				}
		}
	}
	void out()
	{
		for (int i = 0; i < height; ++i)
		{
			a[i].sort();
			for (int j = 0; j < maxlen - a[i].last; ++j)
				cout << 0 << " ";
			cout << endl;
		}
	}
};
int main()
{
	matrix M, N, K;
	M.fill();
	N.fill();
	M.out();
	N.out();
	K.sum(M, N);
	K.out();
	system("pause");
	return 0;
}