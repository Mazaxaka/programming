#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
	FILE *fp;
	fp = fopen(argv[1], "r");
	//fp = fopen("inp.txt", "r");
	int a[52] = {0};
	char c;
	fscanf(fp,"%c", &c);
	do
	{	
			if (c >= 'A' && c <= 'Z')
			{
				++a[c - 65];
			}
			else if (c >= 'a' && c <= 'z')
			{
				++a[c - 71];
			}
			fscanf(fp,"%c", &c);
	}while (!feof(fp));
	for (int i = 0; i < 26; ++i)
	{
		if (a[i] != 0)
		{
			cout << char(i + 65) << ": " << a[i] << endl;
		}
	}
	for (int i = 26; i < 52; ++i)
	{
		if (a[i] != 0)
		{
			cout << char(i + 71) << ": " << a[i] << endl;
		}
	}
	system("pause");
	return 0;
}