#include <iostream>

using namespace std;
struct stec
{
	stec *Next, *Head;
	int value;
	stec()
	{
		Head = NULL;
	}
};

void push(int val, stec **MyStec)
{
	stec *temp = new stec;
	temp -> value = val;
	temp -> Next = (*MyStec) -> Head;
	(*MyStec) -> Head = temp;
}
int pop(stec *MyStec)
{
	int ans;
	if (MyStec -> Head != NULL)
	{
		stec *temp = MyStec -> Head -> Next; 
		//cout << MyStec -> Head -> value << endl;
		ans = MyStec -> Head -> value;
		delete MyStec -> Head;
		MyStec -> Head = temp;
		return ans;
	}
	return 0;
}
void clean(stec *MyStec)
{
	while (MyStec -> Head != NULL)
	{
		stec *temp = MyStec -> Head -> Next;
		delete MyStec -> Head;
		MyStec -> Head = temp;
	}
}

int main(int argc, char *argv[])
{
	stec *MyStec = new stec;
	for (int i = 0; i < argc; ++i)
	{
		if (argv[i][0] >= '0' && argv[i][0] <= '9')
		{
			push(atoi(argv[i]), &MyStec);
		}
		else if(argv[i][0] == '+')
		{
			int a, b;
			b = pop(MyStec);
			a = pop(MyStec);
			push(a + b, &MyStec);
		}
		else if(argv[i][0] == '-')
		{
			int a, b;
			b = pop(MyStec);
			a = pop(MyStec);
			push(a - b, &MyStec);
		}
		else if(argv[i][0] == '*')
		{
			int a, b;
			b = pop(MyStec);
			a = pop(MyStec);
			push(a * b, &MyStec);
		}
		else if(argv[i][0] == '/')
		{
			int a, b;
			b = pop(MyStec);
			a = pop(MyStec);
			push(a / b, &MyStec);
		}
	}
	cout << pop(MyStec) << endl;
	system("pause");
	return 0;
}