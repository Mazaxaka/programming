#include <iostream>
#include <locale>
using namespace std;
struct complex;
complex com_dual(complex a);
complex com_comp_dual(complex a);
struct complex
{
    double Re, Im;
    void out()
    {
        if (Im >= 0) cout << Re << " + " << Im << "i" << endl;
		else cout << Re << " " << Im << "i" << endl;
    }
	complex com_dual()
	{
		complex ans;
		ans.Re = Re;
		ans.Im = -Im;
		return ans;
	}
	complex operator+(complex& b)
	{
		complex ans;
		ans.Re = Re + b.Re;
		ans.Im = Im + b.Im;
		return ans;
	}
	complex operator-(complex & b)
	{
		complex ans;
		ans.Re = Re - b.Re;
		ans.Im = Im - b.Im;
		return ans;
	}
	complex operator*(const complex& const b) const
	{
		complex ans;
		ans.Re = Re * b.Re - Im * b.Im;
		ans.Im = Im * b.Re + Re * b.Im;
		return ans;
	}
	complex operator/(const complex& const b) const
	{
		complex ans;
		ans.Re = Re * b.Re + Im * b.Im;
		ans.Im = Im * b.Re - Re * b.Im;

		ans.Re /= (b.Re * b.Re + b.Im *b.Im);
		ans.Im /= (b.Re * b.Re + b.Im *b.Im);
		return ans;
	}
};
complex create_cmplx(double Re, double Im)
{
	complex ans;
	ans.Re = Re;
	ans.Im = Im;
	return ans;
}

complex com_comp_dual(complex a)
{
    complex ans;
    ans = a * a.com_dual();
    return ans;
}


int main()
{
    setlocale(LC_ALL, "Russian");
    complex Com;
	double a, b;
    cin >> a >> b;
    Com = create_cmplx(a,b);
	Com.out();
	double c, d;
	complex Com2;
	cin >> c >> d;
	Com2 = create_cmplx(c,d);
	Com2.out();
	(Com + Com2).out();
	(Com - Com2).out();
	(Com / Com2).out();
	(Com * Com2).out();
	Com.com_dual().out();
	Com2.com_dual().out();
	system("pause");
    return 0;
}
