#include <iostream>
using namespace std;

int step(int a, int n)
{
	int ans = 1;
	while (n)
	{
		if (n % 2)
		{
			ans *= a;
			--n;
		}
		else 
		{
			a *= a;
			n /= 2;
		}
	}
	return ans;
}
int main()
{
	int a, n;
	cin >> a >> n;
	cout << step(a,n);
	system("pause");
	return 0;
}