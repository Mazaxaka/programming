#include <iostream>
using namespace std;
void start(int per[], int n)
{
	for (int i = 0; i <= n; ++i)
	{
		per[i] = i;
		//cout << i << " ";
	}
	//cout << endl;
}
void permutation(int n) 
{
	static int per[100];
	static int zapNumber = 0;
	if (zapNumber == 0) 
	{
		start(per, n);
		++zapNumber;
	}
	int stat[100];
	for (int i = 0; i < 100; ++i)
	{
		stat[i] = 0;
	}
	int ip = n;
	while (per[ip] < per[ip - 1]) --ip;
	for (int j = 0; j < ip - 1; ++j)
	{
		stat[per[j]] = 1;
	}
	int k = 1;
	while (stat[per[ip - 1] + k] != 0) ++k;
	per[ip - 1] += k;
	stat[per[ip - 1]] = 1;
	for (int j = ip; j <= n; ++j)
	{
		int t = 0;
		while (stat[t] != 0) ++t;
		per[j] = t;
		stat[t] = 1;
	}
	//for (int i = 0; i <= n; ++i)
		//cout << per[i] << " ";
	//cout << endl;
}
int main()
{
	int n;
	cin >> n;
	int perNumb = 1;
	for (int i = 2; i <= n; ++i)
		perNumb*= i;
	--perNumb;
	for (int i = 0; i < perNumb; ++i)
		permutation(n - 1);
	system("pause");
	return 0;
}
