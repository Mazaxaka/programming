#include <iostream>
#include <string.h>
using namespace std;
class StrDigit;
class Str
{
protected:
	int length;
    char* place;
public:
    Str ()
    {
        place = 0;
        length = 0;
    }
    Str (char *p)
    {
        int now = 0;
        int l = strlen(p);
        place = new char[l+2];
        while (p[now] != '\0')
        {
            place[now] = p[now++];
        }
        place[now] = p[now];
        length = now;
    }
    Str (const Str &b)
    {
	//	free();
		place = new char[b.length + 1];
		for (int i = 0; i < b.length; ++i)
		{
			place[i] = b.place[i];
		}
		length = b.length;
		place[length] = '\0';
    }
    void free()
    {
        if(place)
            delete[] place;
		place = NULL;
		length = NULL;
    }
    ~Str()
    {
        free();
    }
	friend void copy(StrDigit &a, const Str &b);
	char* out()
	{
		return place;
	}
    virtual Str operator+(const Str &s)
    {
        char *p;
		p = new char[length + s.length + 1];
        Str ans;
		for (int i = 0; i < length; ++i)
        {
            p[i] = place[i];
        }
        for (int i = 0; i < s.length; ++i)
        {
            p[length + i] = s.place[i];
        }
        p[length + s.length] = '\0';
		ans.free();
        ans.place = p;
        ans.length = length + s.length;
        return ans;
    }
	void operator=(const Str &b)
	{
		free();
		place = new char[b.length + 1];
		for (int i = 0; i < b.length; ++i)
		{
			place[i] = b.place[i];
		}
		length = b.length;
		place[length] = '\0';
		
	}
	void operator=(const char *b)
	{
		free();
		length = strlen(b);
		place = new char[length + 1];
		for (int i = 0; i <= length; ++i)
		{
			place[i] = b[i];
		}
		
	}
    bool operator==(const Str& s) const
    {
        if (length != s.length)  return false;
        else
            for (int i = 0; i < length; ++i)
                if (place[i] != s.place[i]) return false;
        return true;
    }
	bool operator!=(const Str& s) const
    {
        if (length != s.length) return true;
        else
            for (int i = 0; i < length; ++i)
                if (place[i] != s.place[i]) return true;
        return false;
    }
	virtual bool operator>(const Str& b) const
    {
		if (strcmp(place, b.place) > 0) return true;
		else return false;

    }
	bool operator>=(const Str& b) const
    {
       if (strcmp(place, b.place) >= 0) return true;
		else return false;
    }
	virtual bool operator<(const Str& b) const
    {
       if (strcmp(place, b.place) < 0) return true;
		else return false;
    }
	bool operator<=(const Str& b) const
    {
        if (strcmp(place, b.place) <= 0) return true;
		else return false;
    }
};
class StrDigit :public Str
{
public:
	using Str::operator=;
	StrDigit()
	{
		length = NULL;
		place = NULL;
	}
	friend void copy(StrDigit &a, const Str &b);
	char* out()
	{
		return place;
	}
	StrDigit(long long input)
	{
		long long temp = input;
		int len;
 		for (len = 0; temp; ++len)
			temp /= 10;
		place = new char[len +1];
		for (int i = len - 1; i >= 0; -- i)
		{
			place[i] = input % 10 + 48;
			input /= 10;
		}
		place[len] = 0;
		length = len;
	}
	long long to_digit()
	{
		long long ans = 0;
		for (int i = 0; i < length; ++i)
		{
			ans += place[i];
			ans *= 10;
		}
		ans /= 10;
		return ans;
	}
	void swap (StrDigit &a, StrDigit &b)
	{
		StrDigit temp;
		temp = a;
		a = b;
		b = temp;
	}
	Str operator+(const Str &s)
	{
		StrDigit a, b, ans;
		a.length = length;
		a.place = new char [length + 1];
		for (int i = 0; i < length; ++i)
			a.place[i] = place[i];
		a.place[length] = '\0'; 
		copy(b, s);
		ans.length = max(a.length, b.length);
		char *p = new char[ans.length + 2];
		if (a < b) swap(a, b);
		bool dolg = false;
		for (int i = ans.length, lena = a.length, lenb = b.length; lena || lenb || i >= 0; --i)
		{
			char temp;
			if (lenb) 
				temp = a.place[--lena] + b.place[--lenb] - '0';
			else if (lena)
				temp = a.place[--lena];
			else 
				temp = '0';

			if (dolg)
			{
				++temp;
				dolg = false;
			}
			if (temp > '9')
			{
				temp -= 10;
				dolg = true;
			}
			p[i] = temp;
		}
		p[ans.length + 1] = '\0';
		if (p[0] == '0')
		{
			ans.place = new char[ans.length + 1];
			for (int i = 1; i < ans.length + 1; ++i)
			{
				ans.place[i - 1] = p[i];
			}
			ans.place[ans.length] = '\0';
			delete [] p;
		}
		else 
		{
			++ans.length;
			ans.place = new char[ans.length + 2];
			for (int i = 0; i < ans.length; ++i)
				ans.place[i] = p[i];
			ans.place[ans.length] = '\0';
			delete [] p;
		}
		return ans;
	}
	bool operator>(const Str& s) const 
	{
		StrDigit ans;
		copy(ans, s);
		if (length > ans.length) return true;
        else if (length == ans.length)
            for (int i = 0; i < length; ++i)
                if (place[i] > ans.place[i]) return true;
				else if (place[i] < ans.place[i]) return false;
        return false;
	}
	bool operator>=(const StrDigit &s) const
    {
        if (length > s.length) return true;
        else if (length == s.length)
            for (int i = 0; i < length; ++i)
                if (place[i] >= s.place[i]) return true;
				else if (place[i] < s.place[i]) return false;
        return false;
    }
	bool operator<(const Str &s) const
	{
		StrDigit ans;
		copy(ans, s);
		if (length < ans.length) return true;
		else if (length == ans.length)
			for (int i = 0; i < length; ++i)
				if (place[i] < ans.place[i]) return true;
				else if (place[i] > ans.place[i]) return false;
		return false;
	}
	bool operator<=(const StrDigit& s) const
    {
        if (length < s.length);
        else if (length == s.length)
            for (int i = 0; i < length; ++i)
                if (place[i] <= s.place[i]) return true;
				else if (place[i] > s.place[i]) return false;
        return false;
    }
};
void copy(StrDigit &a, const Str &b){
	a.length = b.length;
	if (b.place != NULL){
		a.place = new char [a.length + 1];
		for (int i = 0; i <= a.length; ++i)
			a.place[i] = b.place[i];
	}
}
template<typename T>
void quick_sort(T inp[], int from, int to)
{
	int bea_numb = int(rand()) % (to - from) + from;
	T bearing = inp[bea_numb];
	int now_i = from; 
	int now_j = to;
	do
	{
		while (inp[now_i] < bearing)
			++now_i;
		while (inp[now_j] > bearing)
			--now_j;
		if (now_i <= now_j) 
			swap(inp[now_i++], inp[now_j--]);
	}
	while (now_i < now_j);
	if (from < now_j) quick_sort(inp, from, now_j);
	if (now_i < to) quick_sort(inp, now_i, to);
}

int main()
{
	Str *a = 0;
	Str *sum = 0;
	char c;
	int n;
	cin >> c >> n;
	if (c == 's') 
	{
		a = new Str[n];
		sum = new Str [1];
	} 
	else if ('d') 
	{
		a = new StrDigit[n];
		sum = new StrDigit [1];
	}
	else 
	{
		return 0;
	}
	for (int i = 0; i < n; ++i)
	{
		char temp[100];
		cin >> temp;
		a[i] = temp;
		sum[0] = sum[0] + a[i];
	}
	
	quick_sort(a, 0, n-1);

	for (int i = 0; i < n; ++i)
		cout << a[i].out() << " ";
	cout << endl << sum[0].out() << endl;
	if (a) delete[] a;
	system("pause");
    return 0;
}
