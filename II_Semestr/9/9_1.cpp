#include <iostream>

using namespace std;

class A
{
private : 
	int x;
public:
	A () 
	{
		cout << "default constructor of lass A"  << endl;
	}
	A(int a) 
	{
		x = a;
		cout << "given constructor of class A"  << endl;
	}
	~A()
	{
		cout << "destructor of class A"  << endl;
	}

};
class B: private A
{
private:
	int y;
public:
	B () 
	{
		cout << "default constructor of lass B"  << endl;
	}
	B(int a, int b): A(a)
	{
		y = b;
		cout << "given constructor of class B"  << endl;
	}
	~B()
	{
		cout << "destructor of class B"  << endl;
	}
};
class C: private B
{
private:
	int z;
public:
	C () 
	{
		cout << "default constructor of lass C"  << endl;
	}
	C(int a, int b, int c): B(a, b)
	{
		z = c;
		cout << "given constructor of class C" << endl;
	}
	~C()
	{
		cout << "destructor of class C" << endl;
	}
};

int main()
{
	C point(1,2,3);
	C *piont = new C;
	delete piont;
	system("pause");
	return 0;
}