#include <iostream>

using namespace std;
const int cMaxStrLen = 5;
const int cMaxStrNum = 10;


int getline(char *buff, int lim) 
{
	int len = 0;
	char c;
	do 
	{
		c = getchar();
		if (len < lim) 
			buff[len++] = c;
	} while (c != '\n');
	buff[--len] = '\0';
	return len;
}
int readlines(char *lineptr[], int N)
{
	getchar();
	for (int i = 0; i < N; ++i)
	{
		char tmp[1000];
		//cin >> tmp;
		
		getline(tmp, 1000);
		lineptr[i] = new char [strlen(tmp)];
		for (int j = 0; j <= strlen(tmp); ++j)
		{
			*(lineptr[i] + j) = *(tmp + j);
		}
		//cout << tmp << " " << lineptr[i] << endl;
	}
	return 0;
}
void alpha(char *lineptr[], int N)
{
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			if (i != j) 
				if (strcmp(lineptr[i], lineptr[j]) < 0)
				{
					swap(lineptr[i], lineptr[j]);
				}
		}
	}
}
int main()
{
	int N = 0;
	cin >> N;
	//N++;
	char *lineptr[5000];
	readlines(lineptr, N);
	cout << endl << "vi vveli" << endl; 
	for (int i = 0; i < N; ++i)
		cout << lineptr[i] << endl; 
	alpha(lineptr, N);
	cout << endl;
	for (int i = 0; i < N; ++i)
		cout << lineptr[i] << endl; 
	//for (int i = 0; i < N - 2; ++i)
		//delete [] lineptr[i];
	system("pause");
	return 0;
}