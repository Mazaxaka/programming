#include <iostream>

const int cMaxStrLen = 1000;
using namespace std;

void converter(char *inp, char converted[100], char replacement[100], int conlen)
{
	int length = strlen(inp);
	for (int i = 0; i < length; ++i)
		for (int j = 0; j < conlen; ++j)
			if (converted[j] == inp[i]) inp[i] = replacement[j];
}

int getline(char *buff, int lim) 
{
	int len = 0;
	char c;
	do 
	{
		c = getchar();
		if (len < lim) 
			buff[len++] = c;
	} while (c != '\n');
	buff[--len] = '\0';
	return len;
}

int main()
{
	char inp[100], converted[100], replacement[100];
	int conlen;
	cin >> conlen;
	getchar();
	getline(inp, 100);
	//cin >> inp;
	//for (int i = 0; i < conlen; ++i)
		//cin >> converted[i];
		getline(converted, 100);
	//for (int i = 0; i < conlen; ++i)
		//cin >> replacement[i];
		getline(replacement, 100);
	converter(inp, converted, replacement, conlen);
		cout << inp << endl;
	system("pause");
	return 0;
}