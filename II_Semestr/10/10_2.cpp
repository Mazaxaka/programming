#include <iostream>

using namespace std;
double g(int n, double *A)
{
	double sum = 0;
	for (int i = 0; i < n; ++i)
	{
		if (A[i] < 10 || A[i] > 100)
		{
			if (A[i] < 0)
			{
				double trouble = A[i];
				throw trouble;
			}
			throw i;
		}
		sum += A[i];
	}
	return sum / n;
}
double f()
{
	int n;
	cin >> n;
	double *A = new double [n];
	for (int i = 0; i < n; ++i)
		cin >> A[i];
	bool ok = false;
	double ans;
	while (!ok)
	{
		try
		{
			ans = g(n, A);
			ok = true;
			delete [] A;
		}
		catch(int number)
		{
			cout << "Error, enter the element number " << number << " again: ";
			cin >> A[number];
		}
		catch(double &trouble)
		{
			delete [] A;
			throw;
		}
	}
	return ans;

}
int main()
{
	try
	{
		cout << f() << endl;
	}
	catch(double a)
	{
		cout << "Fail :(" << endl;
	}

	system("pause");
	return 0;
}