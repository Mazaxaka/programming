#include <iostream>
//#include <io.h>
using namespace std;
struct FileNotFound
{
	char name[100];
};
struct Overwriting
{
	char name[100];
};
void copy(char* name1, char* name2)
{
	FILE* fp;
	if ((fp = fopen(name1, "rb")) == 0)
	{
		FileNotFound trouble;
		strcpy(trouble.name, name1);
		throw trouble;
	}
	FILE* wr;
	if (wr = fopen(name2, "rb"))
	{
		fclose(wr);
		Overwriting trouble;
		strcpy(trouble.name, name2);
		throw trouble;
	}
	
	wr = fopen(name2, "wb");
	char temp;
	fscanf(fp,"%c", &temp);
	do
	{	
		fprintf(wr,"%c", temp);
		fscanf(fp,"%c", &temp);
	}while (!feof(fp));
	fclose(wr);

}
int main()
{
	bool ok = false;
	char name1[100];
	char name2[100];
	bool name2_ok = false;
	bool name1_ok = false;
	while (!ok)
	try
	{
		if (!name1_ok) cin >> name1;
		if (!name2_ok) cin >> name2;
		name1_ok = true;
		name2_ok = true;
		copy(name1, name2);
		ok = true;
	}
	catch(FileNotFound &e)
	{
		cout << "File not found: " << e.name << endl << "repeat input: ";
		name1_ok = false;

	}
	catch(Overwriting &e)
	{
		cout << "File exists: " << e.name << endl << "repeat input: ";
		name2_ok = false;
	}
	cout << "Job done!" << endl;
	system("pause");
	return 0;
}