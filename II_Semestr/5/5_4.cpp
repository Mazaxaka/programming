#include <iostream>
#include <math.h>
using namespace std;

typedef int (*pFunc)(int);
int func(int a)
{
    static int ans = 0;
        ans += a;
    return ans;
}

void f(int a[], int len, pFunc fu)
{
    for (int i = 0; i < len; ++i)
    {
        a[i] = fu(a[i]);
    }
}





int main()
{
    int n;
    cin >> n;
    int a[100];
    for (int i = 0; i < n; ++i)
        cin >> a[i];
    f(a, n, func);
    for (int i = 0; i < n; ++i)
        cout <<  a[i] << " ";
    return 0;
}
