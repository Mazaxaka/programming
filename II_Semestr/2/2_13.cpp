#include <iostream>

using namespace std;
void multiplication(int matrix[100][100], int vector[100], int *ans, int x, int y)
{
	for (int i = 0; i < x; ++i)
	{
		int nowans = 0;
		for (int j = 0; j < y; ++j)
			nowans += matrix[i][j] * vector[j];
		ans[i] = nowans;
	}
}
int main()
{
	int matrix[100][100];
	int vector[100], ans[100] = {0};
		int x, y;
	cin >> x >> y;
	for (int i = 0; i < x; ++i)
		for (int j = 0; j < y; ++j)
			cin >> matrix[i][j];
	for (int i = 0; i < y; ++i)
		cin >> vector[i];
	multiplication(matrix, vector, ans, x, y);
	cout << "ans = "; 
	for (int i = 0; i < x; ++i)
		cout << ans[i] << " ";
	cout << endl;
	system("pause");
	return 0;
}
