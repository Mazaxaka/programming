#include <iostream>

using namespace std;

int step(int a, int n)
{
	int k = n; 
	int b = 1; 
	int c = a;
	while (k != 0)
	{
		if (k % 2 == 0)
		{
			k /= 2;
			c *= c;
		}
		else 
		{
			k --;
			b *= c;
		}
	}
	return b;
}
int derivative(int a[100], int n, int k, int y)
{
	for (int j = 0; j < k; j ++)	
		for (int i = 0; i < n; i++)
		{
			a[i] = a[i+1] * (i + 1);
		}
	n -= k;
	int ans = 0;
	for (int i = 0; i <= n ; i++)
	{
		ans += step(y, i) * a[i];
	}
	return ans;
}
int main()
{
	int a[100];
	int n, k, y;
	cin >> n >> k >> y;
	for (int i = 0; i <= n; i ++)
		cin >> a[i];
	cout << derivative(a, n, k, y) << endl;
	system("pause");
	return 0;
}