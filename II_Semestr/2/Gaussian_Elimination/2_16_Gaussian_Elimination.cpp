#include <iostream>
#include <math.h>
using namespace std;


 
unsigned int min(unsigned int a, unsigned int b)
{
	if (b <= a) return b;
	else return  a;
}
unsigned int NOD (unsigned int a, unsigned int b)
{
	int r = a % b;
	while (r)
	{
		a = b;
		b = r;
		r = a % b;
	}
	return b;
}
void first_min(unsigned int a, unsigned int b)
{
	if (a > b) swap(a,b);
}
unsigned int NOK (unsigned int a, unsigned int b)
{
	return a * b / NOD(a, b);
}
class frac
{
public :
	int num;
	unsigned int den;

	friend frac& operator+=(frac &left, const frac &right)
	{
		left = left + right;
		return left;
	}
	friend frac& operator-=(frac &left, const frac &right)
	{
		left = left - right;
		return left;
	}
	friend frac& operator*=(frac &left, const frac &right)
	{
		left = left * right;
		return left;
	}
	friend frac& operator/=(frac &left, const frac &right)
	{
		left = left / right;
		return left;
	}
	frac()
	{
		num = 0;
		den = 1;
	}
	frac(int a)
	{
		num = a;
		den = 1;
	}
	frac(int a, unsigned int b)
	{
		num = a;
		den = b;
	}
	void frac_red()
	{
		unsigned int NO = NOD(abs(num), den);
		if (num < 0) 
		{
			unsigned int temp = abs(num);
			num = temp / NO;
			num *= -1;
		}
		else num /= NO;
		den /= NO;
	}
	const frac operator+(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * nok / den + b.num * nok / b.den;
		ans.frac_red();
		return ans;
	}
	const frac operator-(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * int (nok / den) - b.num * int (nok / b.den);
		ans.frac_red();
		return ans;
	}
	const frac operator*(const frac& b)
	{
		frac ans;
		ans.num = num * b.num;
		ans.den = den * b.den;
		ans.frac_red();
		return ans;
	}
	const frac operator/(const frac& b)
	{
		frac ans;
		ans.num = (b.num > 0) ? num * b.den : -num * int(b.den);
		ans.den = den * (b.num > 0) ? b.num : abs(b.num);
		ans.frac_red();
		if (b.num > 0)
		{
			ans.num = num * b.den;
			ans.den = den * b.num;
		}
		else 
		{
			ans.num = -num * b.den;
			ans.den = den * abs(b.num);
		}
		ans.frac_red();
		return ans;
	}
	const void operator=(const frac& b)
	{
		num = b.num;
		den = b.den;
	}
	const void operator=(const int& b)
	{
		num = b;
		den  = 1;
	}
	const bool operator==(const frac& b)
	{
		if (num == b.num && den == b.den) return 1;
		return 0;
	}
	const bool operator==(const int& b)
	{
		if (num == b) return 1;
		else return 0;
	}
	const bool operator<(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * nok / den - b.num * nok / b.den;
		ans.frac_red();
		if (ans.num < 0) return 1;
		else return 0;
	}
	const bool operator<=(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * nok / den - b.num * nok / b.den;
		ans.frac_red();
		if (ans.num <= 0) return 1;
		else return 0;
	}
	const bool operator>(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * nok / den - b.num * nok / b.den;
		ans.frac_red();
		if (ans.num > 0) return 1;
		return 0;
	}
	const bool operator>=(const frac& b)
	{
		frac ans;
		unsigned int nok = NOK(den, b.den);
		ans.den = nok;
		ans.num = num * nok / den - b.num * nok / b.den;
		ans.frac_red();
		if (ans.num >= 0) return 1;
		return 0;
	}
	void show()
	{
		if (den == 1) 
			cout << num;
		else 
			cout << num << "/" << den;
	}
	void swap(frac &b)
	{
		frac temp;
		temp.num = num;
		temp.den = den;
		num = b.num;
		den = b.den;
		b.num = temp.num;
		b.den = temp.den;
	}
	void reverse()
	{
		int a;
		if (num > 0)
		{
			a = den;
			den = num;
			num = a;
		}
		else 
		{
			a = abs(num);
			num = -int(den);
			den = a;
		}
	}
};
const int Max_length = 100;
const int Max_height = 100;
class Gaussian_elimination
{
	frac field[Max_height][Max_length];
	frac answers[Max_length][Max_length];
	int height;
	int length;
	bool matrix_shit;

	void say_you_basis(int string_number, int first_not_null)
	{
		for (int i = first_not_null; i < length - 1; ++i)
		{
			if (answers[length - 1][i] == 0) answers[length - 1][i] = 1;
		}
	}
	int find_max_string(int now_string, int now_column)
	{
		frac max_str = field[now_string][now_column];
		int max_number = now_string;
		for (int i = now_string + 1; i < height; ++i)
		{
			if (field[i][now_column] > max_str)
			{
				max_str = field[i][now_column];
				max_number = i;
			}
		}
		return max_number;
	}
	void swap_string(int first, int second)
	{
		if (first != second)
		{
			frac temp;
			for (int i = 0; i < length; ++i)
			{
				field[first][i].swap(field[second][i]);
			}
		}
	}
	void string_mult(int number, frac factor)
	{
		for (int i = 0; i < length; ++i)
		{
			field[number][i] = field[number][i] * factor;
		}
	}
	void subtract(int from, int what)
	{
		for (int i = 0; i < length; ++i)
		{
			field[from][i] -= field[what][i];
		}

	}
	void subtract_from_lower(int now_number)
	{
		int first_not_NULL = 0;
		for (first_not_NULL; field[now_number][first_not_NULL] == 0; ++first_not_NULL);

		for (int i = now_number + 1; i < height; ++i)
		{
			frac now_frac = field[now_number][first_not_NULL];
			frac change_frac = field[i][first_not_NULL];

			if (!(field[i][first_not_NULL] == 0))
			{
				string_mult(now_number, change_frac);
				string_mult(i, now_frac);

				subtract(i, now_number);

				change_frac.reverse();
				string_mult(now_number, change_frac);
			}
		}
	}
	int find_first_not_null(int string_number)
	{
		int first_not_null;
		for (first_not_null = 0; field[string_number][first_not_null] == 0 && first_not_null < length - 1; ++first_not_null);
		if (first_not_null == length -1) return -1;
		return first_not_null;
	}
public:
	Gaussian_elimination()
	{
		height = NULL;
		length = NULL;
		matrix_shit = false;
	}
	void show()
	{
		for (int i = 0; i < height; ++i)
		{
			for (int j = 0; j < length; ++j)
			{
				field[i][j].show();
				cout << " ";
			}
			cout << endl;
		}
	}
	void input()
	{
		cin >> height >> length;
		for (int i = 0; i < height; ++i)
		{
			for (int j = 0; j < length; ++j)
			{
				int temp;
				cin >> temp;
				field[i][j] = temp;
			}
		}
	}
	void input(char* name)
	{
		FILE *fp;
		fp = fopen(name, "r");
		fscanf(fp, "%d %d", &height, & length);
		for (int i = 0; i < height; ++i)
		{
			for (int j = 0; j < length; ++j)
			{
				int temp;
				fscanf(fp, "%d", &temp);
				field[i][j] = temp;
			}
		}
	}
	/*void string_abrivation(int string_number)
	{
		int nod = NOD(abs(field[string_number][0].num), abs(field[string_number][1].num));
		for (int i = 2; i < length; ++i)
		{
			NOD(abs(field[string_number][0].num), abs(field[string_number][1].num));
		}
	}*/
	void solve()
	{
		for (int i = 0, colomn = 0; i < height; ++i)
		{
			int max_string;
			do 
			{
				max_string = find_max_string(i, colomn);
				++colomn;
			}while (field[max_string][colomn] == NULL);

			swap_string(i, max_string);
			subtract_from_lower(i);
			cout<< endl;
			show();
			
		}
	}
	void make_answers_table()
	{
		
		for (int i = height - 1; i >= 0; --i)
		{
			int not_null = find_first_not_null(i);
			if (not_null != -1 && !(matrix_shit))
			{
				answers[not_null][0] = field[i][length - 1];
				answers[not_null][length - 1] = 2;
				for (int j = not_null + 1; j < length - 1; ++j)
				{
					if (answers[j][length -1] == 0)
					{
						answers[j][length -1] = 1;
						answers[j][j] = 1;
					}
				}
				for (int j = not_null + 1; j < length - 1; ++j)
				{
					for (int k = 0; k < length - 1; ++k)
					{
						frac temp = field[i][j] * answers[j][k];
						answers[not_null][k] -= temp;
					}
				}
				for (int j = 0; j < length -1; ++j)
				{
					answers[not_null][j] /= field[i][not_null]; 
				}
			}
			else if (!(field[i][length -1] == 0))
			{
				matrix_shit = true;
			}
		}
	}
	void partial_solution_out()
	{
		if (!matrix_shit)
		{
			cout << endl << "Partial solution:" << endl;
			for (int i = 0; i < length -1; ++i)
			{
				answers[i][0].show();
				cout << " ";
			}
			cout << endl;
		}
		else 
		{
			cout << endl << "There is no solution" <<endl;
		}
	}
	void basis()
	{
		if (!matrix_shit)
		{
			bool say_basis = true;
			for (int i = 0; i < length - 1; ++i)
			{
				if (answers[i][length - 1] == 1)
				{
					if (say_basis)
					{
						cout << endl << "Basis:" << endl;
						say_basis = false;
					}
					for (int j = 0; j < length -1; ++j)
					{
						answers[j][i].show();
						cout <<  " ";
					}
					cout << endl;
				}
			}
		}
	}
};

int main()
{
	Gaussian_elimination matrix;
	matrix.input("math.txt");
	matrix.show();
	matrix.solve();
	cout << endl << "Output matrix:" << endl;
	matrix.show();
	matrix.make_answers_table();
	matrix.partial_solution_out();
	matrix.basis();
	system("pause");
	return 0;
}

/*
� �������� ������� ��� ����� ��� ��������� ���, ��� ����� �������� ����������� � ���� ����)
		1) �������, ��� � ��� ��� ���� ����������� �������
		2) ����� �������� ��� ������� � ������������ ����, ����� (��� ���������) �������, ������� ��� � ���� �������:
			2.1) �� �������� ���������� � ������� ������ �������, ������� ������������ �������. find_max_string
			2.2) ����������� ��� ������. 
			2.3) ��������� ������ ������ �� ��������� � ������ ����� ��������.
			2.4) �������� �� ������ ������ ������� �������.
			2.5) �������� ����������� �������.
		3) ����� �������� �����.
			3.1) ������� ������ ��������� ������� � ��������� ������
			3.2) �������, ��� ��� �������� �� ��� - ������, ��������, ����� ���������� (��� ���������� �������).
			3.3) ���������� ������� ��� ����, ������� �����
			3.4) ��� ������ ������ ������ ���� ����� ���� ������ �� �������� ��� �����, � ��� ����� - �������� �;
			3.5) ���������� ������ ���������� ����� �����;
		4) �� � �� ���������:)
			4.1) ������� ��������� �������
			4.2) ������� �����
*/