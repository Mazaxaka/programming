#include <iostream>

using namespace std;

int DifNum(int a[3000], int len)
{
	int ans = 0;
	int srav[30000] = {0};
	int k = 0;
	for (int i = 0; i < len; ++i)
	{
		bool sos = true;
		for (int j = 0; j < k; ++j)
		{
			if (srav[j] == a[i]) sos = false;

		}
		if (sos == true)
		{
			srav[k] = a[i];
			++ans;
			++k;
		}
	}
	return ans;
}
int main()
{
	int a[3000];
	int len;
	cin >> len;
	for (int i = 0; i < len; ++i)
	{
		cin >> a[i];
	}
	cout << DifNum(a, len) << endl;
	//system("pause");
	return 0;
}