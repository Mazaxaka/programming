#include <iostream>

using namespace std;

int MaxLength(int a[1000], int len)
{
	int ans = 0;
	int next = 0;
	for (int i = 0; i < len; ++i)
	{
		if (a[i] == next + 1)
		{
			++ans;
			++next;
			next = next % 3;
		}
	}
	return ans;
}

int main()
{
	int a[1000];
	int len;
	cin >> len;
	for (int i = 0; i < len; ++i)
	{
		cin >> a[i];
	}
	cout << MaxLength(a, len) << endl;

	system("pause");
	return 0;
}