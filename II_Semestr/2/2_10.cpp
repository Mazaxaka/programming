#include <iostream>

using namespace std;

int main()
{
	double inp[15][2];
	double max, now;
	for (int i = 0; i < 15; ++i)
		cin >> inp[i][0] >> inp[i][1];
	max = now = 0;
	for (int i = 0; i < 15; ++i)
		for (int j = 0; j < 15; ++j)
		{
			if (i - j) now = sqrt(pow((inp[i][0] - inp[j][0]), 2.0) +  pow((inp[i][1] - inp[j][1]), 2.0));
			if (now > max) max = now;
		}
	cout << max;
	system("pause");
	return 0;
}