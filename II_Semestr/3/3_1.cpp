#include <iostream>

using namespace std;
int  DelEven(int *&arr, int N)
{
	int OddCount = 0;
	for (int i = 0; i < N; ++i)
		if (arr[i] % 2 == 1) OddCount ++;
	int *odd = new int [OddCount];
	int ann = 0;
	for (int i = 0; i < OddCount; ++i)
	{
		while (arr[ann] % 2 == 0) ann++;
		odd[i] = arr[ann];
	}

	//arr = odd;
	return OddCount;
}
int main()
{
	int N;
	cin >> N;
	int *arr = new int [N];
	for (int i = 0; i < N; ++i) 
		arr[i] = rand();
	for (int i = 0; i < N; ++i)
		cout << arr[i] << " ";
	cout << endl;
	int Odd = DelEven(arr,N);
	for (int i = 0; i < Odd; ++i)
	{
		cout << arr[i] << " ";
	}
	delete [] arr;
	cout << endl;
	system("pause");
	return 0;
}
