#include <iostream>
#include "robot_B.h"
using namespace std;

int main()
{
	char go;
	robot_B robo("errorlist.txt");
	cin >> go;
	while (go != '0') 
	{
		try
		{
			robo.write_move(go);
			robo.out();
			cin >> go;
		}
		catch(IllegalCommand &e)
		{
			cout << "Command not found" << endl;
			cout << "Avalible commands:" << endl;
			cout << "  S - South" << endl << "  N - North" << endl << "  E - East" << endl << "  W - West" << endl << "  0 - Exit"<<endl;
			robo.out();
			cin >> go;
		}
		catch(OffTheField &f)
		{
			cout << "You can't leave the battlefield. Deserters will be shot!" << endl;
			robo.out();
			cin >> go;
		}
	}
	system("pause");
	return 0;
}