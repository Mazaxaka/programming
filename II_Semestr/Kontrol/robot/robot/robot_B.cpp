#include "robot_B.h"


robot::robot()
{
	x = y = 0;
}
robot::robot(int a, int b)
{
	x = a;
	y = b;
}
void robot::out()
{
	cout << "Now you at: x = " << x << ", y = " << y << endl;  
}
void robot::move(char direct)
{
	switch (direct)
	{
	case 'N':
		{
			if (y == 10) 
			{
				OffTheField trouble;
				trouble.illegal_direction = direct;
				trouble.pos_x = x;
				trouble.pos_y = y;
				throw trouble;
				break;
			}
			++y;
			break;
		}
	case 'S':
		{
			if (y == 0) 
			{
				OffTheField trouble;
				trouble.illegal_direction = direct;
				trouble.pos_x = x;
				trouble.pos_y = y;
				throw trouble;
				break;
			}
			--y;
			break;
		}
	case 'W':
		{
			if (x == 0) 
			{
				OffTheField trouble;
				trouble.illegal_direction = direct;
				trouble.pos_x = x;
				trouble.pos_y = y;
				throw trouble;
				break;
			}
			--x;
			break;
		}
	case 'E':
		{
			if (x == 10) 
			{
				OffTheField trouble;
				trouble.illegal_direction = direct;
				trouble.pos_x = x;
				trouble.pos_y = y;
				throw trouble;
				break;
			}
			++x;
			break;
		}
	default:
		{
			IllegalCommand trouble;
			trouble.illegal_direction = direct;
			throw trouble;
			break;
		}
	}
}

robot_B::robot_B(char* name)
{
	fp = fopen(name, "w");
}
robot_B::robot_B(int a, int b, char* name)
{
	fp = fopen(name, "w");
}
robot_B::~robot_B()
{
	fclose(fp);
}
void robot_B::IllegalCommand_report(IllegalCommand report_information)
{
	fprintf(fp, "Illegal_Command_error \n");
	fprintf(fp, "  IllegalCommand: %c \n", report_information.illegal_direction);
	fprintf(fp, "----------------------\n");
}
void robot_B::OffTheField_report(OffTheField report_information)
{
	fprintf(fp, "OffTheField_error \n");
	fprintf(fp, " direction: %c \n x_now: %d \n y_now: %d \n", report_information.illegal_direction, report_information.pos_x, report_information.pos_y);
	fprintf(fp, "----------------------\n");
} 
void robot_B::write_move(char direct)
{
	try
	{
		move(direct);
	}
	catch(IllegalCommand &f)
	{
		IllegalCommand_report(f);
		throw f;
	}
	catch(OffTheField &f)
	{
		OffTheField_report(f);
		cout << "ger 2" << endl;
		throw f;
	}
}
