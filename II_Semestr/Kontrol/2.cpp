#include <iostream>
using namespace std;

struct IllegalCommand
{
	char illegal_direction;
};
struct OffTheField
{
	int pos_x, pos_y;
	char illegal_direction;
};
class robot
{
public:
	int x, y;
	robot()
	{
		x = y = 0;
	}
	robot(int a, int b)
	{
		x = a;
		y = b;
	}
	void out()
	{
		cout << "Now you at: x = " << x << ", y = " << y << endl;  
	}
	void move(char direct)
	{
		switch (direct)
		{
		case 'N':
			{
				if (y == 10) 
				{
					OffTheField trouble;
					trouble.illegal_direction = direct;
					trouble.pos_x = x;
					trouble.pos_y = y;
					throw trouble;
					break;
				}
				++y;
				break;
			}
		case 'S':
			{
				if (y == 0) 
				{
					OffTheField trouble;
					trouble.illegal_direction = direct;
					trouble.pos_x = x;
					trouble.pos_y = y;
					throw trouble;
					break;
				}
				--y;
				break;
			}
		case 'W':
			{
				if (x == 0) 
				{
					OffTheField trouble;
					trouble.illegal_direction = direct;
					trouble.pos_x = x;
					trouble.pos_y = y;
					throw trouble;
					break;
				}
				--x;
				break;
			}
		case 'E':
			{
				if (x == 10) 
				{
					OffTheField trouble;
					trouble.illegal_direction = direct;
					trouble.pos_x = x;
					trouble.pos_y = y;
					throw trouble;
					break;
				}
				++x;
				break;
			}
		default:
			{
				IllegalCommand trouble;
				trouble.illegal_direction = direct;
				throw trouble;
				break;
			}
		}
	}
};
class robot_B
{
	FILE* fp;
	int x, y;
	public:
	robot_B(char* name)
	{
		fp = fopen(name, "w");
		x = y = 0;
	}
	robot_B(int a, int b, char* name)
	{
		fp = fopen(name, "w");
		x = a;
		y = b;
	}
	~robot_B()
	{
		fclose(fp);
	}
	void IllegalCommand_report(IllegalCommand report_information)
	{
		fprintf(fp, "Illegal_Command_error \n");
		fprintf(fp, "  IllegalCommand: %c \n", report_information.illegal_direction);
		fprintf(fp, "----------------------\n");
	}
	void OffTheField_report(OffTheField report_information)
	{
		fprintf(fp, "OffTheField_error \n");
		fprintf(fp, " direction: %c \n x_now: %d \n y_now: %d \n", report_information.illegal_direction, report_information.pos_x, report_information.pos_y);
		fprintf(fp, "----------------------\n");
	}
	void out()
	{
		cout << "Now you at: x = " << x << ", y = " << y << endl;  
	}
	void move(char direct)
	{
		switch (direct)
		{
		case 'N':
			{
				if (y == 10) 
				{
					OffTheField trouble;
					trouble.illegal_direction = direct;
					trouble.pos_x = x;
					trouble.pos_y = y;
					OffTheField_report(trouble);
					throw trouble;
					break;
				}
				++y;
				break;
			}
		case 'S':
			{
				if (y == 0) 
				{
					OffTheField trouble;
					trouble.illegal_direction = direct;
					trouble.pos_x = x;
					trouble.pos_y = y;
					OffTheField_report(trouble);
					throw trouble;
					break;
				}
				--y;
				break;
			}
		case 'W':
			{
				if (x == 0) 
				{
					OffTheField trouble;
					trouble.illegal_direction = direct;
					trouble.pos_x = x;
					trouble.pos_y = y;
					OffTheField_report(trouble);
					throw trouble;
					break;
				}
				--x;
				break;
			}
		case 'E':
			{
				if (x == 10) 
				{
					OffTheField trouble;
					trouble.illegal_direction = direct;
					trouble.pos_x = x;
					trouble.pos_y = y;
					OffTheField_report(trouble);
					throw trouble;
					break;
				}
				++x;
				break;
			}
		default:
			{
				IllegalCommand trouble;
				trouble.illegal_direction = direct;
				IllegalCommand_report(trouble);
				throw trouble;
				break;
			}
		}
	}
};
class error_reporter
{
	FILE *fp;
public:
	error_reporter(char *name)
	{
		fp = fopen(name, "at");
	}
	~error_reporter()
	{
		fclose(fp);
	}
	void IllegalCommand_report(IllegalCommand report_information)
	{
		fprintf(fp, "Illegal_Command_error \n");
		fprintf(fp, "  IllegalCommand: %c \n", report_information.illegal_direction);
		fprintf(fp, "----------------------\n");
	}
	void OffTheField_report(OffTheField report_information)
	{
		fprintf(fp, "OffTheField_error \n");
		fprintf(fp, " direction: %c \n x_now: %d \n y_now: %d \n", report_information.illegal_direction, report_information.pos_x, report_information.pos_y);
		fprintf(fp, "----------------------\n");
	}
};
int main()
{
	char go;
	robot_B robo("errorlist.txt");
	cin >> go;
	while (go != '0') 
	{
		try
		{
			robo.move(go);
			robo.out();
			cin >> go;
		}
		catch(IllegalCommand &e)
		{
			cout << "Command not found" << endl;
			cout << "Avalible commands:" << endl;
			cout << "  S - South" << endl << "  N - North" << endl << "  E - East" << endl << "  W - West" << endl << "  0 - Exit"<<endl;
			robo.out();
			cin >> go;
		}
		catch(OffTheField &f)
		{
			cout << "You can't leave the battlefield. Deserters will be shot!" << endl;
			robo.out();
			cin >> go;
		}
	}
	system("pause");
	return 0;
}