#include <iostream>

using namespace std;

int main()
{
	int a, *p;
	char c, *pc;
	double d, *pd;
	void *pv;
	cout<<sizeof(p)<<endl; //4
	cout<<sizeof(pc)<<endl; //4
	cout<<sizeof(pd)<<endl; //4
	cout<<sizeof(pv)<<endl; //4
	return 0;
}