#include <iostream>

using namespace std;

char abc(unsigned long long N)
{
	unsigned long long  current = N, step = 2;
	int action = 0;
	while (step < N) 
	{
		step *= 2;
	}
	while (current != 1)
	{
		step /= 2;
		if (current > step) 
		{
			action ++;
			current -= (step);
		}
	}
	char Ans = 97 + action % 3;
	return Ans;
}
int main()
{
	for (int i = 1; i < 30000; i++)
		cout << abc(i);
	cout << endl;

	//system("pause");
	return 0;
}