#include <iostream>

using namespace std;

int f(int N)
{
	if (N == 0 || N == 1) return N;
	else if (N % 2 == 0) return f(N / 2);
	else return f(N / 2) + f(N / 2 + 1);
}

int main()
{
	int n;
	cin >> n;
	cout << f(n) << endl;
	system("pause");
	return 0;
}