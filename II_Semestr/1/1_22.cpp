#include <iostream>
using namespace std;

int NOD(int a, int b)
{
	int ans, q, r;
	if (a < b) 
	{
		int temp = a;
		a = b;
		b = temp;
	}
	r = b;
	while (r)
	{
		ans = r;
		q = a / b;
		r = a % b;
		a = b;
		b = r;
	}
	return ans;
}
int main()
{
	int a, b;
	cin >> a >> b;
	cout << NOD(a, b) << endl;
	//system("pause");
	return 0;
}