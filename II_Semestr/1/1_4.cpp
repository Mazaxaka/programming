#include <iostream>

using namespace std;
int main()
{

	int a;
	char b;
	bool c;
	short d;
	long e;
	float f;
	double g;
	long double h;
	int i;
	cout << &a << endl;
	cout << " " << sizeof(a) << endl;
	cout << "  " << (int*)&a - (int *)&b <<endl;
	cout << (void*)&b << endl;
	cout << " " << sizeof(b) << endl;
	cout << &c << endl;
	cout << " " << sizeof(c) << endl;
	cout << &d << endl;
	cout << " " << sizeof(d) << endl;
	cout << &e << endl;
	cout << " " << sizeof(e) << endl;
	cout << &f << endl;
	cout << " " << sizeof(f) << endl;
	cout << &g << endl;
	cout << " " << sizeof(g) << endl;
	cout << &h << endl;
	cout << " " << sizeof(h) << endl;
	cout << &i << endl;
	cout << " " << sizeof(i) << endl;

return 0;
}