#include <iostream>

using namespace std;

unsigned long long CountZeros(int N)
{
	unsigned long long ans = 0;
	while (N != 1)
	{
		N /= 5;
		ans += N;
	}
	return ans;
}

int main()
{
	int N;
	cin >> N;
	cout << CountZeros(N) << endl;
	return 0;
}