//
//  3_4.cpp
//  Home
//
//  Created by Твердый Евгений on 29.11.13.
//  Copyright (c) 2013 Твердый Евгений. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string> // чтобы использовать << >>

const int TABLE_SIZE = 179;

using namespace std;

class hash_table
{
public:
    hash_table()
    {
        for (int i = 0; i < 8; ++i)
        {
            _a[i] = rand() % TABLE_SIZE;
        }
        _table.resize(TABLE_SIZE);
    }
    void add(string &str)
    {
        int has = hash(str);
        vector<vElement>::iterator location = find(_table[has].begin(), _table[has].end(), vElement(str));
        if (location != _table[has].end())      // matching element found
        {
            ++(*location);
        }
        else                                    // no matching element was found
        {
            _table[has].push_back(vElement(str));
        }
    }

    int hash (string &str)
    {
        int ans = 0;
        for (int i = 0; i < 8; ++i)
        {
            if (str[i] != '\0')
                ans += _a[i] * str[i];
            else
                break;
        }
        ans %= TABLE_SIZE;
        return ans;
    }
    
    friend ostream &operator<<(ostream &os, hash_table &obj);

private:
    struct vElement
    {
        string str;
        int count;
        vElement(string &str)
        {
            this->str = str;
            count = 1;
        }
        bool operator==(const vElement &obj)
        {
            return (str == obj.str);
        }
        
        int operator++()
        {
            return ++count;
        }
    };
    int                      _a[8];
    vector<vector<vElement>> _table;
};


ostream &operator<<(ostream &os, hash_table &obj)
{
    for (int i = 0; i < TABLE_SIZE; ++i)
    {
        vector<hash_table::vElement>::iterator it = obj._table[i].begin();
        for (; it != obj._table[i].end(); ++it)
        {
            cout << (*it).str << " " << (*it).count << "\n";
        }
    }
    return os;
}


int main()
{
    hash_table myTable;
    ifstream fin("tale.rtf");
    if (fin.is_open())
    {
        string tmp;
        char temp[30];
        char t;
        
        while (!fin.eof())
        {
            int now = 0;
            do
            {
                fin.get(t);
                temp[now++] = t;
            }
            while ((('a' <= t && t <= 'z') || ('A' <= t && t <= 'Z')) && !fin.eof());
            temp[--now] = '\0';
            if (now != 0)
            {
                tmp = temp;
                myTable.add(tmp);
            }
        }
    }
    cout << myTable;
    
    return 0;
}