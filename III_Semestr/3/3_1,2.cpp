#include <iostream>

using namespace std;

template <class T>
class deque
{
    struct node                                 // элемент дека
    {
        node *next, *prev;
        T data;
    };
    
public:
    class iterator;
    deque();
    ~deque();
    void push_back (T);
    void push_front(T);
    T pop_back();
    T pop_front();
    long size();
    bool is_empty();
    iterator begin();
    iterator end();
    iterator erase(iterator location);
    
private:
    long count;
    node *head, *tail;
};

template <class T>
class deque<T>::iterator
{
    friend class deque;

public:
    iterator() : element(0){}
    bool operator==(const iterator &it) const;  // сравнение итераторов
    bool operator!=(const iterator &it) const;
    iterator& operator++();                     // переход к следующему элементу
    iterator& operator--();                     // переход к предыдущему элементу
    T& operator*() const;                       // получение ссылки на поле data элемента списка
    node* operator->() const;                   // получение указателя на хранимый элемент списка

private:
    iterator(node *_element) : element(_element){}
    
    node *element;
};

template <class T>
deque<T>::deque()
{
    count = 0;
    tail = head = new node;
    tail->next = tail->prev = NULL;
}

template <class T>
deque<T>::~deque()
{
    while (head != tail)
    {
        pop_back();
    }
    delete tail;
}

template <class T>
void deque<T>::push_back (T data)
{
    node *tmp = new node;
    tmp->next = NULL;
    tmp->prev = tail;
    tail->data = data;
    tail->next = tmp;
    tail = tmp;
    ++count;
}

template<class T>
void deque<T>::push_front(T data)
{
    node *tmp = new node;
    tmp->next = head;
    tmp->prev = NULL;
    tmp->data = data;
    head->prev = tmp;
    head = tmp;
    ++count;
}

template<class T>
T deque<T>::pop_back()
{
    T tmp = 0;
    if (!is_empty())
    {
        tmp = tail->prev->data;
        tail = tail->prev;
        delete tail->next;
        tail->next = NULL;
        --count;
    }
    return tmp;
}

template <class T>
T deque<T>::pop_front()
{
    T tmp = 0;
    if (!is_empty())
    {
        tmp = head->data;
        head = head->next;
        delete head->prev;
        head->prev = NULL;
        --count;
    }
    return tmp;
}

template <class T>
long deque<T>::size()
{
    return count;
}

template <class T>
bool deque<T>::is_empty()
{
    return head == tail;
}

template <class T>// сравнение итераторов
bool deque<T>::iterator::operator==(const iterator &it) const
{
    return (element == it->element);
}

template <class T>
bool deque<T>::iterator::operator!=(const iterator &it) const
{
    return (element != it.element);
}

template <class T>
typename deque<T>::iterator& deque<T>::iterator::operator++()
{
    element = element->next;
    return *this;
}

template <class T>
typename deque<T>::iterator& deque<T>::iterator::operator--()
{
    element = element->prev;
    return *this;
}

template <class T>// получение ссылки на поле data элемента списка
T& deque<T>::iterator::operator*() const
{
    //if (element == NULL)// Не забудьте проверить, что element != NULL.
      // return head->data;
    return element->data;
}

template <class T>// получение указателя на хранимый элемент списка
typename deque<T>::node* deque<T>::iterator::operator->() const
{
    return element;
}

template <class T>
typename deque<T>::iterator deque<T>::begin() { 
    return iterator(this->head); 
}

template <class T>
typename deque<T>::iterator deque<T>::end() {
    return iterator(this->tail);
}

template <class T>
typename deque<T>::iterator deque<T>::erase(iterator location) {
    iterator tmp = location->next;
    if (location.element == head)
        head = tmp.element;
    tmp->prev = location->prev;
    if(location->prev)
        location->prev->next = tmp.element;
    delete location.element;
    --count;
    return tmp;
}

int main()
{
    deque<int>::iterator it;
    
    deque<int> deq;
    
    cout << "deque size is " << deq.size() << endl;
    
    for (int i = 0; i < 10; ++i)
    {
        deq.push_front(2 * i);
        deq.push_back(2 * i + 1);
    }
    cout << "deque size is " << deq.size() << endl;
    for (int i = 0; i < 5; ++i)
    {
        cout << "pop.fron = "<< deq.pop_front() << endl;
        cout << "pop.back = "<< deq.pop_back() << endl;
    }
    cout << "deque size is " << deq.size() << endl;
    
    it = deq.end();
    while (it != deq.begin())
    {
        --it;
        cout << *it << endl;
    }
    cout << "deque size is " << deq.size() << endl;
    
    double max = *(deq.begin());
    for (it = deq.begin(); it != deq.end(); ++it)
    {
        if (*it > max)
            max = *it;
    }
    cout << "The max is " << max << endl;
    
    //Тут начинается 3_2
    for (it = deq.begin(); it != deq.end();)
    {
        if (*it % 2 == 0)
            it = deq.erase(it);
        else
            ++it;
    }
    
    it = deq.end();
    while (it != deq.begin())
    {
        --it;
        cout << *it << endl;
    }
    cout << "deque size is " << deq.size() << endl;
   
    
    return 0;
}

/*
 Реализуйте шаблонный контейнер дек с итератором. При этом должна быть возмож-
 ность как увеличивать, так и уменьшать итератор. В основной программе заполните дек 
 случайными числами, попеременно используя методы push_front() и push_back(). 
 Удалите половину элементов из дека, используя методы pop_front() и pop_back(). 
 Выведите количество оставшихся элементов на экран, далее выведите элементы в 
 порядке, обратном прядку их хранения в деке. Найдите максимальный элемент в деке и 
 выведите его значение на экран.
*/
