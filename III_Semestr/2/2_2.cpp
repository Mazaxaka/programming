// Find all the errors in this code
#include <iostream>

using namespace std;

class point1D {
protected:
	int x;
public:
	point1D(int a = 0) : x(a) {}
	void say_hi() {
		cout << "This function just prints Hi" << endl;
	}
	void set_point(int a){
		x = a;
	}
	point1D operator+(point1D &p1) {
		return point1D(x+p1.x); 
	}
	point1D& operator=(point1D &obj) {
		x = obj.x;
		return *this; 
	}
};

class point2D {
protected:
	int y, z;
public:
	point2D(int a = 0, int b = 0) : y(a), z(b) {}
	void say_hi() {
		cout << "This function just prints Hi" << endl;
	}
	void set_point(int a, int b) {
		y = a;
		z = b;
	}
	point2D operator+(point2D &p2) {
		  return point2D(y + p2.y, z + p2.z);
	}
	point2D& operator=(point2D &obj) {
		y = obj.y;
		z = obj.z;
		return *this; 
	}
};

class point3D : public point1D, public point2D {
public:
	point3D(int a = 0, int b = 0, int c = 0) : point1D(a), point2D(b,c){}
	using point1D::say_hi;
	void set_point(int xx = 0, int yy = 0, int zz = 0){
		point1D :: set_point(xx);
		point2D :: set_point(yy, zz);
	}
	point3D operator+(point3D &p3){
		
		return point3D(x + p3.x, y + p3.y, z + p3.z);
	}
};

int main() 
{
	point1D p1;
	point3D p2, p3, p4;
	p2.say_hi();
	p3.set_point(3);
	p2.set_point(2, 4, 6);
	p4 = p2;
	p4 = p2 + p3;
	system("pause");
	return 0;
}