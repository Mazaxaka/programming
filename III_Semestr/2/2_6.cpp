//
//  2_6.cpp
//  Home_works
//
//  Created by Твердый Евгений on 25.10.13.
//  Copyright (c) 2013 Твердый Евгений. All rights reserved.
//

#include <iostream>
#include <math.h>
using namespace std;

class Expr
{
	int arr[1024 * 256]; //‰Îˇ ÔÓ‚ÂÍË ‡·ÓÚ˚ Ò Ô‡ÏˇÚ¸˛;
    static int exprCount;
public:
    Expr() {++exprCount; }
	virtual void print() const  = 0;
	virtual Expr* copy() const = 0;
	virtual Expr* der() const = 0;
	virtual ~Expr() {--exprCount; }
	virtual double getVal(double point) const = 0;
    static void show_count() {
        cout << endl << "Expressions count = " << exprCount << endl;
    }
};
int Expr::exprCount = 0;

class Const:public Expr
{
	double val;
public:
	Const(double value)
	{
		val = value;
	}
    
	~Const(){}
	void destructor(){}
	void print() const
	{
		cout << val;
	}
    
	Expr *copy() const
	{
		return new Const(val);
	}
    
	Expr *der()const
    
	{
		return new Const(0);
	}
    
	double getVal(double point) const
	{
		return val;
	}
};

class Var:public Expr
{
public:
	Var() {}
    
	void print() const
	{
		cout<< "x";
	}
    
	Expr *copy() const
	{
		return new Var();
	}
    
	~Var(){}
    
	double getVal(double point) const
	{
		return point;
	}
    
	Expr *der() const
	{
		return new Const(1);
	}
};

class Sum:public Expr
{
	Expr *fir, *sec;
public:
	Sum(const Expr *first, const Expr *second)
	{
		fir = first->copy();
		sec = second->copy();
	}
    
	~Sum()
	{
		delete fir;
		delete sec;
	}
    
	void print() const;
    
	Expr *copy() const
	{
		return new Sum(fir, sec);
	}
    
	Expr *der() const;
    
	double  getVal(double point) const
	{
		return fir->getVal(point) + sec->getVal(point);
	}
};

Expr* Sum::der() const
{
    Expr *part1 = fir->der();
    Expr *part2 = sec->der();
    Expr *derivative = new Sum(part1 , part2);
    delete part1;
    delete part2;
    return derivative;
}

void Sum::print() const
{
    cout << "(";
    fir->print();
    cout << " + ";
    sec->print();
    cout << ")";
}

class Mult:public Expr
{
	Expr *fir, *sec;
public:
	Mult(const Expr *first, const Expr *second)
	{
		fir = first->copy();
		sec = second->copy();
	}
    
	~Mult()
	{
		delete fir;
		delete sec;
	}
    
	void print() const;
    
	Expr *copy() const
	{
		return new Mult(fir, sec);
	}
    
	Expr *der() const;
    
	double getVal(double point) const
	{
		return fir->getVal(point) * sec->getVal(point);
	}
};
Expr* Mult::der() const
{
    Expr *part1 = fir->der();
    Expr *part2 = sec->der();
    Expr *part3 = new Mult(part1, sec);
    Expr *part4 = new Mult(fir, part2);
    Expr *derivative = new Sum(part3, part4);
    
    delete part1;
    delete part2;
    delete part3;
    delete part4;
    return derivative;
}

void Mult::print() const
{
    cout << "(";
    fir->print();
    cout << " * ";
    sec->print();
    cout << ")";
}

class Sin:public Expr
{
	Expr *val;
public:
	Sin(const Expr *value)
	{
		val = value->copy();
	}
    
	~Sin()
	{
		delete val;
	}
    
	void print() const
	{
		cout << "Sin(";
		val->print();
		cout << ")";
	}
    
	Expr *copy() const
	{
		return new Sin(val);
	}
    
	Expr *der() const ;
    
	double getVal(double point) const
	{
		return sin(val->getVal(point));
	}
    
};

class Cos:public Expr
{
	Expr *val;
public:
	Cos(const Expr *value)
	{
		val = value->copy();
	}
    
	~Cos()
	{
		delete val;
	}
    
	void print() const;
    
	Expr *copy() const
	{
		return new Cos(val);
	}
    
	Expr *der() const;
    
	double getVal(double point) const
	{
		return cos(val->getVal(point));
	}
};

Expr* Sin::der() const
{
	Expr *part1 = new Cos(val);
	Expr *part2 = val->der();
	Expr *derivative  = new Mult(part1, part2);
	delete part1;
	delete part2;
	return derivative;
}

void Cos::print() const
{
    cout << "Cos(";
    val->print();
    cout << ")";
}

Expr* Cos::der() const
{
    Expr *part1 = new Const(-1);
    Expr *part2 = new Sin(val);
    Expr *part3 = new Mult(part1, part2);
    Expr *part4 = val->der();
    Expr *derivative = new Mult(part3, part4);
    delete part1;
    delete part2;
    delete part3;
    delete part4;
    return derivative;
}


int main()
{
	Expr *v = new Var;
	Expr *c = new Const(2);
	Expr *m = new Mult(c, v);
	Expr *c2 = new Const(7);
	Expr *s = new Sum(v, c2);
	Expr *si = new Sin(s);
	Expr *co = new Cos(m);
	Expr *e;
	si->print();
	cout << endl;
	cout << si->getVal(9);
	cout << endl;
	co->print();
	e = co->der();
	cout << endl;
	e->print();
	cout << endl;
	cout << e->getVal(2);
	cout << endl;
	delete v;
	delete c;
	delete m;
	delete c2;
	delete s;
	delete si;
	delete co;
	delete e;
	system("pause");
	return 0; 
    
}