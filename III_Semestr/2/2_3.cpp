//  2_3.cpp
//  Home_works
//
//  Created by Твердый Евгений on 18.10.13.
//  Copyright (c) 2013 Твердый Евгений. All rights reserved.
//

#include <iostream>
using namespace std;

class spbsuMember
{
    char *name;
public:
    spbsuMember(char memName[])
    {
        name = new char(strlen(memName) + 1);
        strcpy(name, memName);
    }
    void print(){cout << name; }
    virtual void printFull(){}
};

class student : public spbsuMember
{
    bool PhD;
    short year;
public:
    student(char memName[], short yearr = 1, bool isPhD = 0): spbsuMember(memName), PhD(isPhD), year(yearr){}
    void printFull()
    {
        print();
        cout << " is a " << year << " year";
        if (PhD) cout << " PhD";
        cout << " student" << endl;
    }
};

class teacher : public spbsuMember
{
    char courses[15][15];
    int coursesCount;
public:
    teacher(char memName[]) : spbsuMember(memName), coursesCount(0) {}
    void push(char cour[][15], int count)
    {
        for (int i = 0; i < count; ++i)
        {
            strcpy(courses[i], cour[i]);
        }
        coursesCount = count;
    }
    void push(char cour[], ...) // почему это не работает?
    {
        //char **p = &cour;
        va_list uk_arg;
        va_start(uk_arg, cour);
        char **p;
        while ( (p = va_arg(uk_arg, char**))!=0)// && coursesCount < 15)
        {
            strcpy(courses[coursesCount++], *p);
            p++;
        }
        va_end(uk_arg);
    }
    void printFull()
    {
        print();
        cout << " teaches: ";
        for (int i = 0; i < coursesCount; ++i)
        {
            cout << courses[i];
            if (i != coursesCount - 1)
                cout << ", ";
        }
        cout << "." << endl;
    }
    
};

class teacherStudent : public student, public teacher
{
public:
    teacherStudent(char memName[]): student(memName, 30, 1), teacher(memName){}
    void printFull()
    {
        student::printFull();
        teacher::printFull();
    }
};
int main()
{
    teacherStudent Fluor("Flor");
    Fluor.push("matan", "fan", "diffu", 0); //почему это не работает?
    /*char florCourses[3][15];
    strcpy(florCourses[0], "matan");
    strcpy(florCourses[1], "fan");
    strcpy(florCourses[2], "diff");
    Fluor.push(florCourses, 3);*/
    Fluor.printFull();
    return 0;
}
