#include <iostream>
using namespace std;

// Абстрактный класс типа список
class list;
struct listElement{
    listElement *next;  // указатель на следующий элемент списка
	double num;         // число, хранящееся в элементе списка
};
class list {
public:
	listElement *head; // указатель на начало списка
	listElement *tail; // указатель на конец списка
	list() {
		// TODO: опишите конструктор
        head = NULL;
        tail = NULL;
	}
    virtual ~list() {}
	virtual void insert(double x) = 0;	// метод, добавляющий элемент в список
	virtual double extract() = 0;		// метод, извлекающий элемент из списка
};

// Список типа очередь
class queue: public list {
public:
	void insert(double x);
	double extract();
    ~queue();
};

void queue::insert(double x) {
                        // TODO: переопределите виртуальную функцию
                        // как функкцию добавления элемента В КОНЕЦ списка
    if (tail == NULL){
        head = new listElement;
        tail = head;
        tail->num = x;
        
    }
    else{
        listElement *temp = tail;
        tail = new listElement;
        temp->next = tail;
        tail->num = x;
        tail->next = NULL;
    }
    
}

double queue::extract() {
	// TODO: переопределите виртуальную функцию
	// как функцию извлечения элемента ИЗ НАЧАЛА списка
	// NB: не забудьте проверить, что список не пуст
    double ans;
    if (head != NULL){
        ans = head->num;
        listElement* temp = head;
        head = head->next;
        delete temp;
    }
    else {
        cout << "queue is empty!\n";
        ans = 0;
    }
    return ans;
}
queue::~queue(){
    while (head){
        extract();
    }
    cout << "now queue is empty!\n";
}

// Список типа стек
class stack: public list {
	void insert(double x);
	double extract();
    ~stack();
};

void stack::insert(double x) {
                    // TODO: переопределите виртуальную функцию
                    // как функкцию добавления элемента В НАЧАЛО списка
    if (head == NULL){
        head = new listElement;
        tail = head;
        head->num = x;
        head->next = NULL;
    }
    else{
        listElement *temp = new listElement;
        temp->next = head;
        temp->num = x;
        head = temp;
    }
}

double stack::extract() {
                // TODO: переопределите виртуальную функцию
                // как функцию извлечения элемента ИЗ НАЧАЛА списка
                // NB: не забудьте проверить, что список не пуст
    double ans;
    if (head == NULL){
        ans = 0;
        cout << "stack is empty!\n";
    }
    else{
        ans = head->num;
        listElement *temp = head;
        head = head->next;
        delete temp;
    }
    return ans;
}

stack::~stack(){
    while(head){
        extract();
    }
    cout << "now stack is empty!\n";
}

int main() {
	list *p;
	char ch;
	double x;
    
	cout << "Enter s or q if you want to work with stack or queue: ";
	cin >> ch;
    switch (ch) {
        case 's':
            p = new stack;
            break;
        case 'q':
            p = new queue;
        default:
            cout << "Wrong input\n";
            break;
    }
    
	cout << "To stop working enter 'x', \n"
    "to insert an element enter 'i' and a value, \n"
    "to extract an element enter 'e':\n";
	do {
		cin >> ch;
		switch (ch) {
            case 'i':
			{
				cin >> x;
				p->insert(x);
				break;
			}
            case 'e':
			{
				cout << "The extracted element is " << p->extract() << endl;
				break;
			}
            case 'x':
			{
				cout << "Stop working...\n";
				break;
			}
            default:
			{
				cout << "Unknown command. Please try again\n";
				break;
			}
		}
	} while (ch != 'x');
    
	// TODO: добавьте в иерархию классов деструкторы так, чтобы
	// при удалении объекта *p стек или очередь очищались (становились пустыми)
	// и выводилось соответствующее сообщение:
	// now stack is empty
	// или
	// now queue is empty
	delete p;
    
	system("pause");
	return 0;
}