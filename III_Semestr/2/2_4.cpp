//
//  2_4.cpp
//  Home_works
//
//  Created by Твердый Евгений on 25.10.13.
//  Copyright (c) 2013 Твердый Евгений. All rights reserved.
//

#include <iostream>

using namespace std;

class shape
{
public:
    virtual double area() = 0;
};

class triangle : public shape
{
    double a, h;
public:
    triangle(double base, double height): a(base), h(height){}
    double area(){return a * h / 2; }
};

class square : public shape
{
    double a;
public:
    square (double aa): a(aa){}
    double area() {return a*a; }
    
};
class rectangle: public shape
{
    double a, b;
public:
    rectangle(double aa, double bb): a(aa), b(bb){}
    double area() {return a*b; }
};
void bublesort(shape *arr[], int len)
{
    for (int i = 0; i < len; ++i)
    {
        for (int j = 1; j < len; ++j)
        {
            if (arr[j - 1]->area() > arr[j]->area())
                swap(arr[j-1], arr[j]);
        }
    }
}
int main()
{
    triangle *f1 = new triangle(2, 4);
    triangle *f2 = new triangle(1, 5);
    triangle *f3 = new triangle(2, 7);
    square *f4 = new square(3);
    square *f5 = new square(1);
    square *f6 = new square(5);
    rectangle *f7 = new rectangle(3, 4);
    rectangle *f8 = new rectangle(2, 3);
    rectangle *f9 = new rectangle(1, 3);
    shape **arr = new shape* [9];
    arr[0] = f1;
    arr[1] = f2;
    arr[2] = f3;
    arr[3] = f4;
    arr[4] = f5;
    arr[5] = f6;
    arr[6] = f7;
    arr[7] = f8;
    arr[8] = f9;
    for (int i = 0; i < 9; ++i)
        cout << arr[i]->area() << endl;
    bublesort(arr, 9);
    cout << endl;
    for (int i = 0; i < 9; ++i)
        cout << arr[i]->area() << endl;
    cout << "done";
    return 0;
}
