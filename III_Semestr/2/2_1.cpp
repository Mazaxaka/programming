#include <iostream>
using namespace std;

class A { 
 int i; 
public: 
 A(int a) {i = a; cout << "A constructor" << endl;} 
 ~A() { cout << "A destructor" << endl;}
}; 
 
class B { 
 int j; 
public: 
 B(int a) {j = a; cout << "B constructor" << endl;} 
 ~B() {cout << "B destructor" << endl;}
}; 
 
class C: public A, public B { 
 int k; 
public: 
	C(int a, int b, int c) :A(b), B(c) {k = a; cout << "C constructor" << endl;}
	~C(){ cout << "C destructor" << endl;}
}; 

int main()
{
	C (3);
	return 0;
}
