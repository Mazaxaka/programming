#include <iostream>
using namespace std;

class Expr
{
	int arr[1024 * 256]; //��� �������� ������ � �������;
public:
	virtual void print() = 0;
	virtual Expr *copy() = 0;
	virtual Expr *der() = 0;
	virtual ~Expr() {};
	virtual double getVal(double a) = 0;
};

class Const:public Expr
{
	double val;
public:
	Const(double value)
	{
		val = value;
	}

	~Const(){}
	void destructor(){}
	void print()
	{
		cout << val;
	}

	Expr *copy()
	{
		return new Const(val);
	}

	Expr *der()

	{
		return new Const(0);
	}

	double getVal(double a)
	{
		return val;
	}
};

class Var:public Expr
{
public:
	Var() {}

	void print() 
	{
		cout<< "x"; 
	}

	Expr *copy() 
	{
		return new Var(); 
	}

	~Var(){}

	double getVal(double point) 
	{
		return point; 
	}

	Expr *der()
	{
		return new Const(1);
	}
};

class Sum:public Expr
{
	Expr *fir, *sec;
public:
	Sum(Expr *first, Expr *second)
	{
		fir = first->copy();
		sec = second->copy();
	}

	~Sum()
	{
		delete fir;
		delete sec;
	}

	void print()
	{
		cout << "(";
		fir->print();
		cout << " + ";
		sec->print();
		cout << ")";
	}

	Expr *copy()
	{
		return new Sum(fir, sec);
	}

	Expr *der()
	{
		Expr *part1 = fir->der();
		Expr *part2 = sec->der();
		Expr *derivative = new Sum(part1 , part2);
		delete part1;
		delete part2;
		return derivative;
	}

	double  getVal(double point)
	{
		return fir->getVal(point) + sec->getVal(point);
	}
};
class Mult:public Expr
{
	Expr *fir, *sec;
public:
	Mult(Expr *first, Expr *second)
	{
		fir = first->copy();
		sec = second->copy();
	}

	~Mult()
	{
		delete fir;
		delete sec;
	}

	void print()
	{
		cout << "(";
		fir->print();
		cout << " * ";
		sec->print();
		cout << ")";
	}

	Expr *copy()
	{
		return new Mult(fir, sec);
	}

	Expr *der()
	{
		Expr *part1 = fir->der();
		Expr *part2 = sec->der();
		Expr *part3 = new Mult(part1, sec);
		Expr *part4 = new Mult(fir, part2);
		Expr *derivative = new Sum(part3, part4);
		
		delete part1;
		delete part2;
		delete part3;
		delete part4;
		return derivative;
	}

	double getVal(double point)
	{
		return fir->getVal(point) * sec->getVal(point);
	}
};

class Sin:public Expr
{
	Expr *val;
public:
	Sin(Expr *value)
	{
		val = value->copy();
	}

	~Sin()
	{
		delete val;
	}

	void print()
	{
		cout << "Sin(";
		val->print();
		cout << ")";
	}

	Expr *copy()
	{
		return new Sin(val);
	}

	Expr *der();

	double getVal(double point)
	{
		return sin(val->getVal(point));
	}

};

class Cos:public Expr
{
	Expr *val;
public:
	Cos(Expr *value)
	{
		val = value->copy();
	}

	~Cos()
	{
		delete val;
	}

	void print()
	{
		cout << "Cos(";
		val->print();
		cout << ")";
	}

	Expr *copy()
	{
		return new Cos(val);
	}

	Expr *der()
	{
		Expr *part1 = new Const(-1);
		Expr *part2 = new Sin(val);
		Expr *part3 = new Mult(part1, part2);
		Expr *part4 = val->der();
		Expr *derivative = new Mult(part3, part4);
		delete part1;
		delete part2;
		delete part3;
		delete part4;
		return derivative;
	}

	double getVal(double point)
	{
		return cos(val->getVal(point));
	}
};
Expr* Sin::der()
{
	Expr *part1 = new Cos(val);
	Expr *part2 = val->der();
	Expr *derivative  = new Mult(part1, part2);
	delete part1;
	delete part2;
	return derivative;
}
int main()
{
	Expr *v = new Var;
	Expr *c = new Const(2);
	Expr *m = new Mult(c, v);
	Expr *c2 = new Const(7);
	Expr *s = new Sum(v, c2);
	Expr *si = new Sin(s);
	Expr *co = new Cos(m);
	Expr *e;
	si->print();
	cout << endl;
	cout << si->getVal(9);
	cout << endl;
	co->print();
	e = co->der();
	cout << endl;
	e->print();
	cout << endl;
	cout << e->getVal(2);
	cout << endl;
	delete v;
	delete c;
	delete m;
	delete c2;
	delete s;
	delete si;
	delete co;
	delete e;
	system("pause");
	return 0; 
}