#include <iostream>
using namespace std;

class Int
{
	int num;
public:
	Int(int val = 0)
	{
		num = val;
	}
	Int(char val[], int base = 10)
	{
		int len; //����� ����� 
		len = strlen(val);
		num = 0;
		int fac = 1;
		for (int i = len - 1; i + 1; --i)
		{
			int n = 0;
			if ('0' <= val[i] && val[i] <= '9')
				n = int(val[i] - '0');
			else if ('a' <= val[i] && val[i] <= 'z')
				n = int(val[i] - 'a') + 10;
			else if ('A' <= val[i] && val[i] <= 'Z')
				n = int(val[i] - 'A') + 10;
			num += n * fac;
			fac *= base;
		}
	}
	int out()
	{
		return num;
	}
};
int main()
{
	Int a(10);
	Int b("AB", 16);
	cout << a.out() << endl;	//OK
	cout << b.out() << endl;	//OK
	return 0;
}