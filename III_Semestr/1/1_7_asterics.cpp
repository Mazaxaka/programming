#include <iostream>
#include <cstdlib>

using namespace std;

template <class T>
class array
{
	int size;
	T *p;
public:
	array(int num)
	{
		size  = num;
		p = new T[size];
	}
	~array()
	{
		delete p;
	}
	T &put(int i) // ���������� ������ �� ������� ������� � �������� i
	{
		if (i >= 0 && i < size)
			return p[i];
		else 
		{
			cout << "Error!";
			return p[0];
		}
	}
	T get(int i) // ���������� �������� �������� ������� � �������� i
	{
		if (i >= 0 && i < size)
			return p[i];
		else 
		{
			cout << "Error!";
			return p[0];
		}
	}
};
int main() 
{
	array<char> a(10);
	a.put(3) = 'A';
	cout << a.get(3) << endl;
	return 0;
}