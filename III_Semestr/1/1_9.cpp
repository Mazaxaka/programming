#include <iostream>
using namespace std;

class who
{
	char *x;
	int size;
public:
	who(char who[])
	{
		size = strlen(who);
		x = new char[size + 1];
		for (int i = 0; i <= size; ++i)
			x[i] = who[i];
		
		cout << "Creating object who # " << x << endl;
	}
	who(const who &obj)
	{
		size = obj.size;
		x = new char[size + 1];
		for (int i = 0; i <= size; ++i)
		{
			x[i] = obj.x[i];
		}
		cout << "Copying  object who # " << x << endl;  
	}
	~who()
	{
		cout << "Deleting object who # "<< x << endl;
		delete x;
	}
};
who make_who(char name[])
{
	who temp(name);
	return temp;
}
void f_value(who a){}
void f_addres(who &a){}
void f_pointer(who *a){}
int main()
{ 
	who a("Anna");
	who b("Bana");
	who c("Cana");
	make_who("Dana");
	f_value(a);
	f_addres(b);
	f_pointer(&c);
	return 0;
}
