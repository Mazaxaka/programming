// Find the error in this code

#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

class strtype 
{
	char *p;
	int len;
public:
	strtype(char *ptr);
	strtype& operator=(strtype &obj);
	friend istream& operator>>(istream &is, strtype &obj);
	friend ostream& operator<<(ostream &os, strtype &obj);
	~strtype();
	void show();
};

strtype::strtype(char *ptr) 
{
	len = strlen(ptr);
	p = new char[len + 1];
	if (!p) {
		cout << "Error in calling operator new" << endl;
		exit(1);
	}
	strcpy(p, ptr);
}

strtype& strtype::operator=(strtype &obj)
{
	if (p != NULL)
		delete p;
	len = strlen(obj.p);
	p = new char[len + 1];
	for (int i = 0; i <= len; ++i)
	{
		p[i] = obj.p[i];
	}
	return *this;
}
istream& operator>>(istream &is, strtype &obj)
{
	if(obj.p != 0)
		delete obj.p;
	char *C = new char[100];
	is >> C;
	obj.len = strlen(C);
	obj.p = new char[obj.len + 1];
	strcpy(obj.p, C);
	delete C;
	return is;
}
ostream& operator<<(ostream &os, strtype &obj)
{
	os << obj.p;
	return os;
}


strtype::~strtype() 
{
	cout << "Free memory associated with the address " << (void *)p << endl;
	delete[] p;
}

void strtype::show() 
{
	cout << p << " - length: " << len << endl;
}

int main() 
{
	strtype s1("Hello");
	strtype s2("Hi");
	strtype s3("Hey");
	cin >> s3;
	cout << s1 << endl;
	s1 = s2;
	cout << s1 << endl;
	s1 = s3;
	cout << s1 << endl;

	return 0;
}