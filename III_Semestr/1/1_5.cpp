#include <iostream>
using namespace std;

union switcher
{
	short int value;
	char temp[sizeof(short int)];
	switcher(short int val)
	{
		value = val;
		swap(temp[0],temp[1]);
	}
};
int main()
{
	short int a;
	cin >> a;
	cout << a << endl;
	switcher test(a);
	cout << test.value << endl;
	system("pause");
	return 0;
}