#include<iostream>
#include <math.h>
using namespace std;
class Expression
{
	int arr[1024 * 256];
public:
	virtual void print() = 0;
	virtual Expression *copy() = 0;
	virtual Expression *der() = 0;
	virtual double getVal(double a) = 0;
};

class Const :public Expression
{
	double val;
	Expression *derivative;
public:
	Const (double x)
	{
		val = x;
		derivative = NULL;
	}
	Expression *copy()
	{
		return new Const(val);
	}
	~Const()
	{
		if (derivative != NULL)
			delete derivative;
	}
	void print()
	{
		cout << val;
	}
	Expression *der()
	{
		if (derivative == NULL)
			derivative = new Const(0);
		return derivative;
	}
	double getVal(double a)
	{
		return val;
	}
};
class Var :public Expression
{ 
	Expression *derivative;
public:
	Var()
	{
		derivative = NULL;
	}
	void print()
	{
		cout << "x";
	}
	Expression *copy()
	{
		return new Var();
	}
	~Var()
	{
		delete derivative;
	}
	Expression *der()
	{
		if (derivative == NULL)
			derivative = new Const(1);
		return derivative;
	}
	double getVal(double point)
	{
		return point;
	}

};
class Sum : public Expression
{
	Expression *first, *second, *derivative;
public :
	Sum(Expression *f, Expression *s)
	{
		first = f->copy();
		second = s->copy();
		derivative = NULL;
	}
	Expression *copy()
	{
		return new Sum(first, second);
	}
	~Sum() 
	{
		delete first;
		delete second;
		delete derivative;
	}
	void print()
	{
		 cout << "(";
		first->print();
		 cout << "+";
		second->print();
		 cout << ")";
	}
	Expression *der()
	{
		if (derivative == NULL)
			derivative = new Sum(first->der(), second->der());
		return derivative;
	}
	double getVal(double point)
	{
		double ans = first->getVal(point) + second->getVal(point);
		return ans;
	}
};
class Mult : public Expression
{
	Expression *first, *second, *derivative;
public:
	Mult(Expression *f, Expression *s)
	{
		first = f->copy();
		second = s->copy();
		derivative = NULL;
	}
	Expression *copy()
	{
		return new Mult(first, second);
	}
	~Mult() 
	{
		delete first;
		delete second;
		delete derivative;
	}
	void print()
	{
		cout << "(";
		first->print();
		cout << "*";
		second->print();
		cout << ")";
	}
	Expression *der()
	{
		if (derivative == NULL)
		{
			Expression *derivativeFirst = new Mult(first->der(), second);
			Expression *derivativeSecond = new Mult(first, second->der());
			Expression *derivative = new Sum(derivativeFirst, derivativeSecond);
			delete derivativeFirst;
			delete derivativeSecond;
		}
		return derivative;
	}
	double getVal(double point)
	{
		return (first->getVal(point) * second->getVal(point));
	}
};
class Sin : public Expression
{
	Expression *val, *derivative;
public :
	Sin(Expression *a)
	{
		val = a->copy();
		derivative = NULL;
	}
	Expression *copy()
	{
		return new Sin(val);
	}
	~Sin()
	{
		delete val;
		delete derivative;
	}
	void print()
	{
		 cout << "Sin(";
		 val->print();
		 cout << ")";
	}
	Expression* der();
	double getVal(double point)
	{
		return sin(val->getVal(point));
	}
};
class Cos : public Expression
{
	Expression *val, *derivative;
public:
	Cos(Expression *a)
	{
		val = a->copy();
		derivative = NULL;
	}
	Expression *copy()
	{
		return new Cos(val);
	}
	~Cos()
	{
		delete val;
		delete derivative;
	}
	void print()
	{
		cout << "Cos(";
		val->print();
		cout << ")";
	}
	Expression *der()
	{
		if (derivative == NULL)
		{
			Expression *derivativeFirst =  new Const(-1);
			Expression *derivativeSecond = new Sin(val);
			Expression *derivativeThird = new Mult(derivativeSecond, val->der());
			Expression *derivative = new Mult(derivativeFirst, derivativeThird);
			delete derivativeFirst;
			delete derivativeSecond;
			delete derivativeThird;
		}
		return derivative;
	}
	double getVal(double point)
	{
		return cos(val->getVal(point));
	}
};

Expression* Sin::der()
{
	if (derivative == NULL)
	{
		Expression *derivativeFirst =  new Cos(val);
		Expression *derivative = new Mult(derivativeFirst, val->der());
		delete derivativeFirst;
	}
	return derivative;
}
int main()
{
	Expression *v = new Var;
	Expression *c = new Const(2);
	Expression *m = new Mult(c, v);
	Expression *c2 = new Const(7);
	Expression *s = new Sum(v, c2);
	Expression *si = new Sin(s);
	Expression *co = new Cos(m);
	Expression *e;
	si->print();
	cout << endl;
	cout << si->getVal(9);
	cout << endl;
	co->print();
	//e = co->der();
	co->der();
	cout << endl;
	e->print();
	cout << endl;
	cout << e->getVal(2);
	cout << endl;
	delete v;
	delete c;
	delete m;
	delete c2;
	delete s;
	delete si;
	delete co;
	delete e;
	system("pause");
	return 0;
}