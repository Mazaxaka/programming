#include <iostream>
using namespace std;

class numbers
{
	public:
	double a, b;
	numbers(double a1, double b1)
	{
		if (a1 > b1)
		{
			a = a1;
			b = b1;
		}
		else 
		{
			a = b1;
			b = a1;
		}
	}
	virtual double considerX(double x)
	{
		if (x <= b)
			return x;
		else if (x <= a)
		{
			double temp = b;
			b = x;
			return temp;
		}
		else 
		{
			double temp = b;
			b = a;
			a = x;
			return temp;
		}
	}
};
class numbers2 :public numbers
{
public:
	double c;
	numbers2(double a1, double b1, double c1): numbers(a1, b1)
	{
		c = numbers::considerX(c1);
	}
	double considerX(double x)
	{
		if (x <= c)
			return x;
		else 
		{
			double temp = c;
			c = numbers::considerX(x);
			return temp;
		}

	}
}; 
int main()
{
	double a = 1, b = 2, c = 3;
	double x = 4;
	numbers2 num(a, b, c);	
	cout << num.a << " " << num.b << " " << num.c << endl;
	x = num.considerX(x);
	cout << num.a << " " << num.b << " " << num.c << " " << x << endl;
	system("pause");
	return 0;
}