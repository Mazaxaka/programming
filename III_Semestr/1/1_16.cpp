#include <iostream>
#include <cstdlib>

using namespace std;

template <class T>
class array
{
	int size;
	T *p;
public:
	array(int num)
	{
		size  = num;
		p = new T[size];
	}
	~array()
	{
		delete p;
	}
	
	T& operator[](int i)
	{
		if (i >= 0 && i < size)
			return p[i];
		else 
		{
			cout << "Error!";
			return p[0];
		}
	}
	array &operator=(const array &b)
	{
		size = b.size;
		if (p != NULL) 
			delete p;
		p = new char[size + 1];
		for (int i = 0; i <= size; ++i)
		{
			p[i] = b.p[i];
		}
		return *this;
	}
	void operator+=(const array &b)
	{
		int newSize = b.size + size;
		T *ptr = new T[newSize + 1];
		for (int i = 0; i < size; ++i)
			ptr[i] = p[i];
		for (int i = 0; i <= b.size; ++i)
			ptr[size + i] = b.p[i];
		if (p != NULL)
			delete p;
		size = newSize;
		p = ptr;
		ptr = NULL;
	}
};
int main() 
{
	array<char> a(10);
	array<char> b(5);
	a[3] = 'A';
	b[4] = 'B';
	a += b;
	cout << a[3] << a[14] << endl;
	a = b;
	cout << a[4] << endl;
	return 0;
}