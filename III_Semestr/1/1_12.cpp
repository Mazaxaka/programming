#include <iostream>
using namespace std;

class Int;
class Double
{
	double num;
public:
	Double ()
	{
		num = 0;
	}
	Double(double num)
	{
		this->num = num;
	}
	friend Double operator+(const Double &first, const Double &second)
	{
		Double ans(first.num + second.num);
		return ans;
	}
	Double operator%(const Int &second);
	friend ostream& operator<<(ostream &os, Double &obj);
	friend istream& operator>>(istream &is, Double &obj);
};

ostream& operator<<(ostream &os, Double &obj)
{
	os << obj.num;
	return os;
}
istream& operator>>(istream &is, Double &obj)
{
	is >> obj.num;
	return is;
}
class Int
{
	int num;
public:
	Int(int val = 0)
	{
		num = val;
	}
	Int(char val[], int base = 10)
	{
		int len; //����� ����� 
		len = strlen(val);
		num = 0;
		int fac = 1;
		for (int i = len - 1; i + 1; --i)
		{
			int n = 0;
			if ('0' <= val[i] && val[i] <= '9')
				n = int(val[i] - '0');
			else if ('a' <= val[i] && val[i] <= 'z')
				n = int(val[i] - 'a') + 10;
			else if ('A' <= val[i] && val[i] <= 'Z')
				n = int(val[i] - 'A') + 10;
			num += n * fac;
			fac *= base;
		}
	}
	friend Double Double::operator%(const Int &second);
	int out()
	{
		return num;
	}
	friend ostream& operator<<(ostream &os, Int &obj);
	friend istream& operator>>(istream &is, Int &obj);
};
Double Double::operator%(const Int &second)
	{
		int temp = (int) num / second.num;
		double ans = num - temp * second.num;
		return ans;
	}
ostream& operator<<(ostream &os, Int &obj)
{
	os << obj.num;
	return os;
}
istream& operator>>(istream &is, Int &obj)
{
	is >> obj.num;
	return is;
}
int main()
{
	Double a;
	int b;
	cin >> a >> b;
	cout << a << " + " << b << " = " << a + b << endl;
	cout << a << " % " << b << " = " << a % b << endl;
	return 0;
}