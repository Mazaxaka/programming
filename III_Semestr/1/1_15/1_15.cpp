#include <iostream>

#include "Double.h"
using namespace std;


int main()
{
	Double a;
	Int b;
	cin >> a >> b;
	cout << a << " + " << b << " = " << a + b << endl;
	cout << a << " % " << b << " = " << a % b << endl;
	cout << b++;
	cout << " " << ++b << endl;
	return 0;
}