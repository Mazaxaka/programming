#include <iostream>
#include "Int.h"
using namespace std;

class Double
{
	double num;
public:
	Double ();
	Double(double num);
	Double(Int num);
	friend Double   operator+(const Double &first, const Double &obj);
	friend Double   operator%(const Double &first, const Int &second);
	friend ostream& operator<<(ostream &os, Double &obj);
	friend istream& operator>>(istream &is, Double &obj);
};