#include <iostream>
using namespace std;

class Double;
class Int
{
	int num;
public:
	Int(int val = 0);
	Int(char val[], int base = 10);
	int val();
	friend Double operator%(const Double &first, const Int &second);
	friend ostream& operator<<(ostream &os, Int &obj);
	friend istream& operator>>(istream &is, Int &obj);
	friend Int operator++(Int &obj, int notused);
	friend Int& operator++(Int &obj);
};