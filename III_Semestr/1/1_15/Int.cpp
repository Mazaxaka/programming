  #include "Int.h"

Int :: Int(int val)
{
	num = val;
}

Int :: Int(char val[], int base)
{
	int len; //����� ����� 
	len = strlen(val);
	num = 0;
	int fac = 1;
	for (int i = len - 1; i + 1; --i)
	{
		int n = 0;
		if ('0' <= val[i] && val[i] <= '9')
			n = int(val[i] - '0');
		else if ('a' <= val[i] && val[i] <= 'z')
			n = int(val[i] - 'a') + 10;
		else if ('A' <= val[i] && val[i] <= 'Z')
			n = int(val[i] - 'A') + 10;
		num += n * fac;
		fac *= base;
	}
}

ostream& operator<<(ostream &os, Int &obj)
{
	os << obj.num;
	return os;
}

istream& operator>>(istream &is, Int &obj)
{
	is >> obj.num;
	return is;
}

int Int :: val()
{
	return num;
}

Int operator++(Int& obj, int notused) 
{
	Int oldValue(obj.num);
	obj.num++;
	return oldValue;
}

Int& operator++(Int &obj)
{
	obj.num++;
	return obj;
}