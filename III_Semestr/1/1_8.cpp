#include <iostream>
#include <cstdlib>

using namespace std;

template <class T>
class array
{
	int size;
	T *p;
public:
	array(){}
	array(int num)
	{
		size  = num;
		p = new T[size];
	}
	array(array &obj)
	{
		cout << "copy constructor is running" << endl;
		size = obj.get_size();
		p = new T[size];
		for (int i = 0; i < size; ++i)
		{
			p[i] = obj.get(i);
		}
	}
	~array()
	{
		delete p;
	}
	int get_size()
	{
		return size;
	}
	T &put(int i) // ���������� ������ �� ������� ������� � �������� i
	{
		if (i >= 0 && i < size)
			return p[i];
		else 
		{
			cout << "Error!";
			return p[0];
		}
	}
	T get(int i) // ���������� �������� �������� ������� � �������� i
	{
		if (i >= 0 && i < size)
			return p[i];
		else 
		{
			cout << "Error!";
			return p[0];
		}
	}
};
int main() 
{
	array<char> a(10);
	a.put(3) = 'A';
	cout << a.get(3) << endl;
	array<char> b(a);
	cout << b.get(3) << endl;
	system("pause");
	return 0;
}