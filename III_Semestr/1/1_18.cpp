#include <iostream>
#include <math.h>
using namespace std;

class Round
{
	int x, y, r;
public:
	Round(int xx = 0, int yy = 0, int rr = 0): x(xx), y(yy), r(rr){}
	friend ostream &operator<<(ostream &os, const Round &obj);
};

int vecLen(int a, int b, int x = 0, int y = 0)
{
	a -= x; 
	b -= y;
	return int(sqrt(double(a * a + b * b)) + 0.5);
}
ostream &operator<<(ostream &os, const Round &obj)
{
	for (int i = 0; i <= obj.x + obj.r; ++i)
	{
		for (int j = 0; j <= obj.y + obj.r; ++j)
		{
			if (vecLen(i , j , obj.x, obj.y) == obj.r)
				 os << "*   ";
			else os << "    ";
		}
		os << "\n \n";
	}
	return os;
}
int main()
{
	Round r(8, 7, 5);
	cout << r;
	system("pause");
	return 0;
}